#include "view.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    View w;
    w.setGeometry(20, 20, 600, 600);
    w.setWindowState(Qt::WindowMaximized);
    w.show();
    return a.exec();

}
#include <QApplication>

