#ifndef LINK_H
#define LINK_H
#include <QGraphicsLineItem>
#include "pole.h"
#include <QObject>

#include <QGraphicsLineItem>

class Pole;

class Link: public QGraphicsLineItem
{
public:
    enum { Type = UserType + 4 };
    enum LinkType { Rotation, Translation, Scaling };

    Link(LinkType type, Pole *startItem, Pole *endItem,
          QGraphicsItem *parent = nullptr);

    int type() const override { return Type; }
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void setColor(const QColor &color) { myColor = color; }
    Pole *startItem() const { return myStartItem; }
    Pole *endItem() const { return myEndItem; }
    LinkType linkType()const {return myType;}
    void updatePosition();

    std::string getTypeString() const;

    float getAngle() const;
    void setAngle(float value);

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

private:
    LinkType myType;
    Pole *myStartItem;
    Pole *myEndItem;
    float angle;
    QPolygonF arrowHead;
    QPolygonF arrowHead2;
    QColor myColor = Qt::red;
    std::string typeString;
};

#endif // LINK_H


