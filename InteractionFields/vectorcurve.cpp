#include "vectorcurve.h"
#include "node.h"

#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QPointF>
#include <QWidget>
#include <QGraphicsScene>
#include <QPainterPath>
#include <vector>
#include <iostream>
#include <string>
#include <QPainter>
#include <QtGui>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include <QBrush>
#include <QStyleOption>
using namespace std;



/**
 * @brief constructor vector curve
 *
 * @param cp list of the control points of the vector curve
 * @param width_ width of the vector curve
 * @param color_ color of the curve
 * @param parent
 */
VectorCurve::VectorCurve(QList<QPointF> cp, float width_, QGraphicsItem *parent)
    :QGraphicsObject(parent)

{

    width = width_;
    //setFlag(QGraphicsItem::ItemIsMovable);
    arrowSize = 10*width_;
    this->curvePts=cp;

    this->createNodes();
    path = getLinePath();

    //setFlag(QGraphicsItem::ItemIsMovable , true);
    setFlag(QGraphicsItem::ItemIsSelectable , true);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);


}

/**
 * @brief
 *
 * @param path_ the path of the curve (liste of all the oints of the curves)
 * @param numberPoint the number of control points
 * @param width_
 * @param color_
 * @param parent
 */
VectorCurve::VectorCurve(QPainterPath path_, int numberPoint, float width_, QGraphicsItem *parent)
    :QGraphicsObject(parent)
{
    width = width_;
    path = path_;
    this->setCurvePts(path, numberPoint);
    this->createNodes();
    //setFlag(QGraphicsItem::ItemIsMovable , true);
    setFlag(QGraphicsItem::ItemIsSelectable , true);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    //setFlag(QGraphicsItem::ItemIsMovable);
    arrowSize = 10*width_;

}


/**
 * @brief change the curve shape according to bézier curve but not used anymore
 *
 * @param index index of the control point moved
 * @param pt new position of the control point
 */
void VectorCurve::changePointBezier(int index, QPointF pt)
{
    getControlPts()[index] = pt;
    getNodes()[index]->setPos(pt);
    QPainterPath newPath = QPainterPath(getNodes()[0]->pos());

    int i = 1;

    while(i < getNodes().size()){

        if( i == index ){
           if(index == getNodes().size()-1){
                newPath.cubicTo(this->getNodes()[index]->pos(), pt, getNodes()[index]->pos());
                break;
           }
           else{
               newPath.cubicTo(getNodes()[index]->pos(), pt, getNodes()[index+1]->pos());
               i +=2;
           }

        }

        else{
            newPath.lineTo(getNodes()[i]->pos());
            i++;

        }


    }
    /*
    if(index < getControlPts().size()-1){
        path.moveTo(getControlPts()[i]);
        newPath.cubicTo(this->curvePts[index], pt, curvePts[index+1]);
        int j = index+2;
        while(j<getControlPts().size()-1){
            newPath.lineTo(getControlPts()[j]);
            j++;
        }


        newPath.cubicTo(this->curvePts[index-1], this->curvePts[index], this->curvePts[index+1]);
        this->curvePts[index]=pt;
        int j = index+2;
        while(j<getControlPts().size()){
            newPath.lineTo(getControlPts()[j]);
            j++;
        }
    }
    else{
        newPath.cubicTo(this->curvePts[index], pt, curvePts[index]);
    }*/


    setPath(newPath);
    setCurvePts(path, getControlPts().size()-1);

    //getNodes()[index]->setPos(path.pointAtPercent(t));
    updateCurvePts(path, index);
    update();

    //adjustNodes();

}

/**
 * @brief change the curve when one control point change
 * the vector curve is a list of points linked by straight lines, moving one point displaces the one or two lines linked
 *
 * @param index
 * @param pt
 */
void VectorCurve::changePointLine(int index, QPointF pt)
{
    getControlPts()[index] = pt;
    getNodes()[index]->setPos(pt);
    QPainterPath newPath = QPainterPath(getNodes()[0]->pos());

    int i = 1;

    while(i < getNodes().size()){
            newPath.lineTo(getNodes()[i]->pos());
            i++;
    }


    setPath(newPath);
    curvePts[index]=pt;
    update();

    //adjustNodes();

}




/**
 * @brief
 *
 * @return QList<QPointF>
 */
QList<QPointF> VectorCurve::getControlPts() const
{
    return this->curvePts;

}

/**
 * @brief
 *
 * @param cp
 */
void VectorCurve::setCurvePtsListe(QList<QPointF> cp)
{
    this->curvePts=cp;
}

/**
 * @brief bounding rect: when the mouse pressed inside this rectangle, the controle curve is selected
 *
 * @return QRectF
 */
QRectF VectorCurve::boundingRect() const
{
    // Adding the arrows to the bounding rect
    QLineF line(curvePts.end()[-2], curvePts.back());
    double angle = std::atan2(-line.dy(), line.dx());

    QPointF destArrowP1 = curvePts.back() + QPointF(sin(angle - M_PI / 3) * arrowSize,
                                              cos(angle - M_PI / 3) * arrowSize);
    QPointF destArrowP2 = curvePts.back() + QPointF(sin(angle - M_PI + M_PI / 3) * arrowSize,
                                              cos(angle - M_PI + M_PI / 3) * arrowSize);
    QPainterPath boundingPath = getLinePath();
    boundingPath.addPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);
    return (boundingPath.boundingRect());
}

/**
 * @brief design of the control curve when it is painted on the renderarea scene
 *the color is gray
 * @param painter
 * @param option
 * @param widget
 */
void VectorCurve::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    (void) widget;
    (void) option;

        QPen pen = QPen(Qt::darkGray, width, Qt::SolidLine);
        painter->setPen(pen);
        QBrush brush = QBrush(Qt::darkGray);
        brush.setStyle(Qt::SolidPattern);

        painter->drawPath(getLinePath());

        // Draw the arrows
        QLineF line(getControlPts().end()[-2], getControlPts().back());
        double angle = std::atan2(-line.dy(), line.dx());

        QPointF destArrowP1 = getControlPts().back() + QPointF(sin(angle - M_PI / 3) * arrowSize,
                                                  cos(angle - M_PI / 3) * arrowSize);
        QPointF destArrowP2 = getControlPts().back() + QPointF(sin(angle - M_PI + M_PI / 3) * arrowSize,
                                                  cos(angle - M_PI + M_PI / 3) * arrowSize);
        for(auto &node : getNodes() ){
            node->setZValue(1);
         }

        this->setZValue(1);
        painter->setBrush(brush);
        painter->drawPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);
        brush.setStyle(Qt::NoBrush);
        painter->setBrush(brush);
        if (isSelected()) {
            painter->setPen(QPen(Qt::red, width, Qt::DashLine));
            QPainterPath myPath = getLinePath();
            myPath.translate(0, 4.0*width);
            painter->drawPath(myPath);
            myPath.translate(0,-8.0*width);
            painter->drawPath(myPath);
            for(auto &node : getNodes() ){
                node->setZValue(2);
             }


        }
        //setCurvePts(path, getControlPts().size());
        //createNodes();
        /*
        adjustNodes();
        for(int i =0; i<int(this->nodes.size()); i++){
            nodes[i]->setPos(getControlPts()[i]);
            nodes[i]->paint(painter, option,widget);

        }
        */
        //adjustNodes();
        /*
        for(int i=0 ; i < int(this->curvePts.size()); i++){
            if(getSelection()==i){
                painter->setPen(QPen(Qt::magenta, 6, Qt::SolidLine, Qt::RoundCap,
                                     Qt::RoundJoin));
                painter->drawPoint(this->curvePts[i]);
            }
            else{
                painter->setPen(QPen(Qt::red, 6, Qt::SolidLine, Qt::RoundCap,
                                     Qt::RoundJoin));
                //QGraphicsEllipseItem *el = new QGraphicsEllipseItem(QRectF(this->curvePts[i].x()-3, this->curvePts[i].y()+3, 6,6));
                painter->drawPoint(this->curvePts[i]);
                painter
                //painter->drawEllipse(el);

            }

        }*/


}

/**
 * @brief
 *
 * @param painter
 */
void VectorCurve::changerColor(QPainter *painter){
    QPen pen = QPen(Qt::green,5 , Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin);
    painter->setPen(pen);
    painter->drawPath(getPath());
    pen = QPen(Qt::red,13 , Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin);
    painter->setPen(pen);
    for(int i=0 ; i < int(this->curvePts.size()); i++){
        painter->drawPoint(this->curvePts[i]);
    }
}





//void VectorCurve::setCurvePts(vector<QPointF> vp){
//    this->curvePts()= vp;
//}

//void VectorCurve::addPt(QPointF p){
//    this->curvePts().push_back(p);
//    if(this->curvePts().size()>10){
//        simplify

//    }

//}

/**
 * @brief create the control points of the vector curve
 * select a points of the path avery t percent according to the number of control points desired
 * @param path the path of the vecrot curve
 * @param numberPoint the number of points we wants
 */
void VectorCurve::setCurvePts(QPainterPath path, int numberPoint) {

    QList<QPointF> L  = *  new QList<QPointF>;
    for(int i = 0; i<=numberPoint; i++){
        qreal t = float(i)/numberPoint;
        L.append(path.pointAtPercent(t)); //the points are updated with the point at each t percent of the path

    }

setCurvePtsListe(L);

}

/**
 * @brief update the control curves TO CHECK!
 *if one control point has moved we change the list of control points
 * @param path  path of the curve
 * @param index  index of the point that has been moved
 */
void VectorCurve::updateCurvePts(QPainterPath path, int index) {

    QList<QPointF> L  = *  new QList<QPointF>;
    for(int i = 0; i<curvePts.size(); i++){
        if(i == index){ // if the index is found, find the position according to the path
            qreal t = float(index)/(curvePts.size()-1);
            L.append(path.pointAtPercent(t));
        }
        else{
            L.append(curvePts[i]); //the other points are the same than before
        }


    }

setCurvePtsListe(L);

}




/**
 * @brief create the nodes of the vector curve at each control points
 *
 */
void VectorCurve::createNodes(){
    nodes.clear();

    for(int i = 0; i<getControlPts().length(); i++){
        Node *node = new Node(getControlPts()[i]);
        node->setIndex(i);
        node->setCurve(this);

        nodes.push_back(node);


    }
}

//void VectorCurve::updateCurve(){
//    int length = this->curvePts.size(); //lenght of the points list
//    QPainterPath p = QPainterPath(this->curvePts[0]);

//    int a = 1;
//    while (a < length){

//        p.cubicTo(this->curvePts[a], this->curvePts[a+1], this->curvePts[a+2]);
//        a=a+3;


//    }

//    setCurve(p, this->curvePts);
//}

/**
 * @brief
 *
 * @return QPainterPath
 */
QPainterPath VectorCurve::getPath() const
{
    return path;
    /*
    int length = this->curvePts.size(); //lenght of the points list
    QPainterPath p = QPainterPath(this->curvePts[0]);

    int a = 1;
    while (a < length){

        p.cubicTo(this->curvePts[a], this->curvePts[a+1], this->curvePts[a+2]);
        a=a+3;


    }
    return p;
    */
}

void VectorCurve::setPath(QPainterPath path_)
{
    path=path_;
}

/**
 * @brief
 *
 * @return QPainterPath
 */
QPainterPath VectorCurve::getLinePath() const
{
    int length = this->curvePts.size();
    QPainterPath p = QPainterPath(this->curvePts[0]);
    for(int i=1; i <length; i++){
        p.lineTo(curvePts[i]);
    }
    return p;
}
/**
 * @brief
 *
 * @return int
 */
int VectorCurve::getSelection() const
{
    return selection;
}

/**
 * @brief
 *
 * @param value
 */
void VectorCurve::setSelection(int value)
{
    selection = value;
}





/**
 * @brief remove a node of the nodes list
 *
 * @param node
 */
void VectorCurve::removeNode(Node *node){
     nodes.erase(std::remove(nodes.begin(), nodes.end(), node), nodes.end());
}

/**
 * @brief remove all nodes
 *
 */
void VectorCurve::removeNodes()
{
    // need a copy here since removeNodes() will
    // modify the nodes container
    const auto nodesCopy = nodes;
    for (Node *node : nodesCopy) {
        node->getCurve()->removeNode(node);
        //scene()->removeItem(node);
        delete node;
    }
}



/**
 * @brief
 *
 * @param curve
 * @return bool VectorCurve::operator
 */
bool VectorCurve::operator==( const VectorCurve &curve)const
{
    if(getControlPts().size()!= curve.getControlPts().size()|| getWidth()!=curve.getWidth() ){
        return false;
    }
    else{
        bool isEqual = true;
        for(int i =0; i<getControlPts().size(); i++){
            if (getControlPts()[i] != curve.getControlPts()[i]){
                isEqual = false;
            }
        }
        return isEqual;
    }

}

/**
 * @brief realign the nodes and the control points : should always have the same positions
 *
 *
 */
void VectorCurve:: adjustNodes(){
    float numberPoint = getNodes().size()-1;
    for(int i =0; i<=numberPoint; i++){
        qreal t = float(i)/numberPoint;
        nodes[i]->setManualChange(false);
        nodes[i]->setPos(path.pointAtPercent(t));
        nodes[i]->setManualChange(true);
    }
}


/**
 * @brief
 *
 * @return float
 */
float VectorCurve::getWidth() const
{
    return width;
}

/**
 * @brief
 *
 * @param value
 */
void VectorCurve::setWidth(float value)
{
    width = value;
}

/**
 * @brief
 *
 * @return std::vector<Node *>
 */
std::vector<Node *> VectorCurve::getNodes() const
{
    return nodes;
}

/**
 * @brief
 *
 * @param value
 */
void VectorCurve::setNodes(std::vector<Node *> value)
{
    nodes = value;
}
