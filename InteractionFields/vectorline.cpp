#include "vectorline.h"


#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QPointF>
#include <QWidget>
#include <QGraphicsScene>
#include <QPainterPath>
#include <vector>
#include <iostream>
#include <string>
#include <QPainter>
#include <QtGui>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include <QBrush>
#include <QStyleOption>
using namespace std;



VectorLine::VectorLine(QGraphicsItem *parent)
{
    setFlag(QGraphicsItem::ItemIsSelectable, true);

    setFlag(ItemSendsGeometryChanges);
}

VectorLine::VectorLine(const QPointF &sPoint, const QPointF &ePoint,float width_,QGraphicsItem *parent)
{
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(ItemSendsGeometryChanges);

    startPoint =sPoint;
    endPoint =ePoint;
    width = width_;
    QPainterPath linePath = QPainterPath(getStartPoint());
    arrowSize = 10*width_;

}

QPointF VectorLine::getStartPoint() const
{
    return startPoint;
}

void VectorLine::setStartPoint(const QPointF &value)
{
    startPoint = value;
}

QPointF VectorLine::getEndPoint() const
{
    return endPoint;
}

void VectorLine::setEndPoint(const QPointF &value)
{
    endPoint = value;
}

QRectF VectorLine::boundingRect() const
{
    const float xS = startPoint.x();
    const float yS =startPoint.y();
    const float xE = endPoint.x();
    const float yE = endPoint.y();

    return QRectF(startPoint, QSize(xE-xS, yE-yS));
}

void VectorLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    (void) widget;
    (void) option;

        QColor colorMag = QColor();
        colorMag.setHsv(220,70+coeffColor,250);
        QPen pen = QPen(colorMag, width, Qt::SolidLine);
        painter->setPen(pen);
        QBrush brush = QBrush(colorMag);
        painter->setBrush(brush);
        QLineF line(startPoint, endPoint);
        painter->drawLine(line);

        // Draw the arrows

        double angle = std::atan2(-line.dy(), line.dx());

        QPointF destArrowP1 = getEndPoint()+ QPointF(sin(angle - M_PI / 3) * arrowSize,
                                                  cos(angle - M_PI / 3) * arrowSize);
        QPointF destArrowP2 = getEndPoint()+ QPointF(sin(angle - M_PI + M_PI / 3) * arrowSize,
                                                  cos(angle - M_PI + M_PI / 3) * arrowSize);



        painter->drawPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);

        if (isSelected()) {
            painter->setPen(QPen(Qt::red, width, Qt::DashLine));
            QLineF myLine = QLineF(startPoint, endPoint);
            myLine.translate(4.0*width, 4.0*width);
            painter->drawLine(myLine);
            myLine.translate(-8.0*width,-8.0*width);
            painter->drawLine(myLine);
        }




}

float VectorLine::getCoeffColor() const
{
    return coeffColor;
}

void VectorLine::setCoeffColor(float value)
{
    coeffColor = value;
}

float VectorLine::getWidth() const
{
    return width;
}

void VectorLine::setWidth(float value)
{
    width = value;
}
float VectorLine::getLength(){
    Vector2D vect =Vector2D(endPoint.x() - startPoint.x(), endPoint.y() - startPoint.y());
    return vect.magnitude();
}



//! [3]
void VectorLine::updatePosition()
{
    //    QLineF line(mapFromItem(myStartItem, 0, 0), mapFromItem(myEndItem, 0, 0));
    //    setLine(line);
}


//! [3]
