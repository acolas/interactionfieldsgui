#ifndef VECTORCURVE_H
#define VECTORCURVE_H

#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QPointF>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsObject>
#include <QPainterPath>
#include <vector>
#include <iostream>
#include <string>
#include <QtGui>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QApplication>
#include <QGraphicsSceneMouseEvent>


class Node;

class VectorCurve : public QGraphicsObject

{


public:

    enum { Type = UserType + 7 };
    VectorCurve(QList<QPointF> controlPts, float width, QGraphicsItem *parent = nullptr) ;
    VectorCurve(QPainterPath path,int numberPoint,float width,QGraphicsItem *parent = nullptr);
    void changePointBezier(int index, QPointF p);
    void changePointLine(int index, QPointF p);
    QList<QPointF> getControlPts() const;


    void setCurvePtsListe(QList<QPointF> cp);
    void setCurvePts(QPainterPath path, int numberpoints);
    void updateCurvePts(QPainterPath path, int index);

    int getSelection() const;
    void setSelection(int value);

    void changerColor(QPainter *painter);
    QPainterPath getLinePath() const;
    QRectF boundingRect() const override;
    std::vector<QLineF>  *lines;
    QPainterPath getPath() const;

    void setPath(QPainterPath path_);
    void DrawNodes(QPainter painter);


    std::vector<Node *> getNodes() const;
    void setNodes(std::vector<Node *> value);

    void createNodes();
    float getWidth() const;
    void setWidth(float value);

    void adjustNodes();
    int type() const override { return Type; }
    bool operator==(const VectorCurve &other) const;



    void removeNodes();
    void removeNode(Node *node);
protected:

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;
private:

    void updateCurve();
    int selection;
    float width;
    qreal arrowSize = 1;
    QPainterPath path;
    std::vector<Node*> nodes;
    QList<QPointF> curvePts;




};




#endif // VECTORCURVE_H
