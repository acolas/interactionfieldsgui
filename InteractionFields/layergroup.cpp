#include "layergroup.h"

#include "pole.h"
#include "link.h"
#include "node.h"
#include <QGraphicsItemGroup>
#include <fstream>
#include <QPointF>
#include <QWidget>
#include <QCursor>
#include <QPainterPath>
#include <vector>
#include <array>

using namespace std;



/**
 * @brief add a pole to the group layer
 *
 * @param pole to add
 */
void LayerGroup::addPole(Pole *pole)
{
    poles.push_back(pole);
}

/**
 * @brief add a link to the group layer
 *
 * @param link to add
 */
void LayerGroup::addLink(Link *link)
{
    links.push_back(link);
}

/**
 * @brief remove a pole
 *
 * @param pole to remove
 */
void LayerGroup::removePole(Pole *pole)
{

    poles.erase(std::remove(poles.begin(), poles.end(), pole), poles.end());
    removeFromGroup(pole);
}

/**
 * @brief
 *
 * @param link
 */
void LayerGroup::removeLink(Link *link)
{

    links.erase(std::remove(links.begin(), links.end(), link), links.end());
    removeFromGroup(link);
    previousPos = pos();


}


/**
 * @brief constructor
 *
 * @param name_ name of the field, will be used to save it after
 * @param id_ id of the field
 * @param parametric if the field is parametric or not
 * @param relatif
 * @param parent
 */
LayerGroup::LayerGroup(QString name_, size_t id_,bool parametric, bool relatif, QGraphicsItem *parent)
{
    setFlag(QGraphicsItem::ItemIsSelectable , true);

    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIsMovable, false);
    name = name_;
    isParametric = parametric;
    isRelative = relatif;
    previousPos = pos();
    isGridVisible = true;
    parameter =0.0f;
    hasSource = false;
    combinationWeight = 0.5;
    id = id_;
}

/**
 * @brief  constructor
 *
 * @param name_ name of the field
 * @param weight weight to combine it with other fields
 * @param id_ id of the field
 * @param parent
 */
LayerGroup::LayerGroup(QString name_, double weight, size_t id_, QGraphicsItem *parent)
{
    setFlag(QGraphicsItem::ItemIsSelectable , true);

    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIsMovable, false);
    name = name_;
    previousPos = pos();
    isGridVisible = true;
    parameter =0.0f;
    isRelative = false;
    isParametric = false;
    hasSource = false;
    id = id_;
    combinationWeight = weight;
}




/**
 * @brief delete the items of the group
 *
 */
void LayerGroup::clearGroup()
//! [9] //! [10]
{

    poles.clear();
    links.clear();

    update();
}

/**
 * @brief combine a groupe with another one: work in progress
 *
 * @param layerB layer to combine with
 */
void LayerGroup::combine(LayerGroup *layerB){
     QRectF combinationRect = getCurrentLayer()->getBorder().intersected(layerB->getCurrentLayer()->getBorder());
     double height = getCurrentLayer()->getBorder().height();
     double width = getCurrentLayer()->getBorder().width();
     QPointF start = getCurrentLayer()->getBorder().bottomLeft();

     for (int i = 0; i*height/(float)getCurrentLayer()->getMatrix()->getRows()< height; i++){

         for(int j = 0; j*width/(float)getCurrentLayer()->getMatrix()->getCols()< width; j++){
             double y = start.y() - (float)i*height/(float)getCurrentLayer()->getMatrix()->getCols();
             double x = start.x()+ (float) j *width/(float)getCurrentLayer()->getMatrix()->getRows();
             double yf = y - height/(float)getCurrentLayer()->getMatrix()->getCols();
             double xf = x + width/(float)getCurrentLayer()->getMatrix()->getRows();

             auto rect = QRectF(QPointF(x,y), QPointF(xf,yf));
             if (combinationRect.contains(rect)){
                 Vector2D vectorA =getCurrentLayer()->getMatrix()->at(i,j);
                 Vector2D vectorB = layerB->getCurrentLayer()->getMatrix()->at(layerB->getCurrentLayer()->getCase(rect.center())[0],layerB->getCurrentLayer()->getCase(rect.center())[1]) ;
                 //getCurrentLayer()->getMatrix()->setAt(i,j, (vectorA.magnitude()*combinationWeight+vectorB.magnitude()*layerB->getCombinationWeight())*(vectorA.getnormalized()+vectorB.getnormalized()));
                 //layerB->getCurrentLayer()->getMatrix()->setAt(layerB->getCurrentLayer()->getCase(rect.center())[0],layerB->getCurrentLayer()->getCase(rect.center())[1], Vector2D(0.0,0.0));
             }
        }
     }

}


void LayerGroup::setBorderRect(const QRectF &value){
    borderRect=value;
}
/**
 * @brief clear the entire group: items + layerviewers
 *
 */
void LayerGroup::clear(){

    //! [9] //! [10]
        /*
        layers[layerID]->getCurrentLayer()->clearLayer();

        layers[layerID]->clearGroup();
        scene()->removeItem(layers[layerID]);
        scene()->removeItem(layers[layerID]->getCurrentLayer());
        if(!scene()->children().isEmpty())
            scene()->clear();
        scene()->addItem(layers[layerID]);
        scene()->addItem(layers[layerID]->getCurrentLayer());

        modified = true;*/
        for(auto item :getCurrentLayer()->getVectCurves() ){
            scene()->removeItem(item);
            getCurrentLayer()->removeVectorCurve(item);
        }

        getCurrentLayer()->clearLayer();
        for(auto item: childItems()){
            scene()->removeItem(item);
            delete (item);
        }
        update();

}
/**
 * @brief add the group as an item to the render area scene
 *
 * @param painter
 * @param option
 * @param widget
 */
void LayerGroup::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (isSelected()) {
        if(flags()& ItemIsMovable)
            painter->setPen(QPen(Qt::blue, 0.2, Qt::DashLine));
        else
            painter->setPen(QPen(Qt::red, 0.2, Qt::DashLine));
    }
    else{
        painter->setPen(QPen(Qt::gray, 0.1));
    }
    painter->drawRect(boundingRect());


}

/**
 * @brief the weight to combine it with another field
 *
 * @return float
 */
float LayerGroup::getCombinationWeight() const
{
    return combinationWeight;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setCombinationWeight(float value)
{
    combinationWeight = value;
}

/**
 * @brief check if the grid is visible
 *
 * @return bool
 */
bool LayerGroup::getIsGridVisible() const
{
    return isGridVisible;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setIsGridVisible(bool value)
{
    isGridVisible = value;
}


/**
 * @brief construct the first lines of the txt field files.
 * check in the list of all the poles and writte the position of the source
 * check for all the links and writte the datas in the first line
 *
 * @return string the first line of the txt file of this layerGroup
 */
string LayerGroup::setMatrixSource()
{
    QList<QGraphicsItem *> items = scene()->items();
    for (QGraphicsItem *item : qAsConst(items)) {
                if (item->type() == Pole::Type) {
                        if(qgraphicsitem_cast<Pole *>(item)->getIsSource()){
                            //array<int,2> source = getCase(item->scenePos());
                            Vector2D posSource = Vector2D(qgraphicsitem_cast<Pole *>(item)->pos().x(),qgraphicsitem_cast<Pole *>(item)->pos().y());
                            //matrix->setSource(source);
                            hasSource=true;
                            //Pole *linkAt;
                            string firstLine = std::to_string(posSource.x()-getCurrentLayer()->getBorder().topLeft().x()) + ";" + std::to_string(posSource.y()-getCurrentLayer()->getBorder().topLeft().y()) ;
                            for (auto &link : qgraphicsitem_cast<Pole *>(item)->getLinks()){

                                 firstLine += ";"+link->getTypeString()+ ";"+ std::to_string(link->endItem()->pos().x()-getCurrentLayer()->getBorder().topLeft().x())+";"+ std::to_string(link->endItem()->pos().y()-getCurrentLayer()->getBorder().topLeft().y());
                                 /*
                                if(link->startItem()==item){
                                    sourceLine +=";"+ link->getTypeString()+ ";"+ std::to_string(link->endItem()->pos().x())+";"+ std::to_string(link->endItem()->pos().y());
                                    linkAt = link->endItem();
                                }

                                else{
                                    sourceLine +=";"+ link->getTypeString()+ ";s;" + link->startItem()->getTypeString()+";"+  std::to_string(static_cast<int>(link->endItem()->getId()));
                                    linkAt = link->startItem();
                                }
                                if(link->linkType()==Link::Rotation){

                                    sourceLine+=";" + std::to_string(counterClockwiseAngle(Vector2D(0, -1), Vector2D(linkAt->scenePos().x(),linkAt->scenePos().y() )- Vector2D(item->scenePos().x(), item->scenePos().y())));
                                }*/
                                /*
                                else if (link->linkType()==Link::Translation){
                                    sourceLine +=
                                }*/



                            }
                            firstLine+="\n";
                         return firstLine;
                        }


                }

           }
            return "";


}
/**
 * @brief  hide or show the control vectors
 *
 */
void LayerGroup::hideVectors(){
    if(getCurrentLayer()->getVectCurves().size()>0){
        if (isVisible() &&  getCurrentLayer()->getVectCurves()[0]->isVisible() ){
            getCurrentLayer()->hideCurves();
        }
        else if(isVisible() &&  !getCurrentLayer()->getVectCurves()[0]->isVisible() ){
            getCurrentLayer()->showCurves();
        }



       }


}

/**
 * @brief  hide or show the items of the layers (poles, links, curves)
 *
 */
void LayerGroup::hideLayer(){
    if (isVisible()){
        hide();
        getCurrentLayer()->hideCurves();
        hidePoles();
        hideLinks();
    }
    else{
        show();
        getCurrentLayer()->showCurves();
        showPoles();
        showLinks();
    }

}
/**
 * @brief override itemCHange
 * update the position of the control points of the control curves when one nodes is moved
 * @param change
 * @param value
 * @return QVariant
 */
QVariant LayerGroup::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    switch (change) {

    case ItemPositionHasChanged:
        for(auto &layer: interactionFields){
             layer.second->setBorder(layer.second->getBorder().translated(pos()-previousPos));
            for (auto &curve: layer.second->getVectCurves()){
                for (auto &node: curve->getNodes())
                    node->setPos(node->pos() + (pos()-previousPos));
            }
        }
        for (auto &pole : getPoles())
            pole->setPos(pole->pos()+ (pos()-previousPos));
        //borderRect = getCurrentLayer()->getBorder().translated(2*(previousPos-pos()));
        previousPos=pos();

    default:
        break;
    };
     return QGraphicsItem::itemChange(change, value);

}


/**
 * @brief hide the poles
 *
 */
void LayerGroup::hidePoles(){
    for(auto &pole : getPoles()){

        pole->hide();
    }
}

/**
 * @brief show the poles
 *
 */
void LayerGroup::showPoles()
{
    for(auto &pole : getPoles()){

        pole->show();
    }
}

/**
 * @brief
 *
 */
void LayerGroup::hideLinks()
{
    for(auto &link : getLinks()){

        link->hide();
    }
}
/**
 * @brief
 *
 */
void LayerGroup::showLinks()
{
    for(auto &link : getLinks()){

        link->show();
    }
}
/**
 * @brief
 *
 * @return QList<Pole *>
 */
QList<Pole *> LayerGroup::getPoles() const
{
    return poles;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setPoles(const QList<Pole *> &value)
{
    poles = value;
}

/**
 * @brief  the list of the links contained in this grouplayer
 *
 * @return QList<Link *>
 */
QList<Link *> LayerGroup::getLinks() const
{
    return links;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setLinks(const QList<Link *> &value)
{
    links = value;
}


/**
 * @brief  map of the layer viewer, the ky is the value of the parameter and the element is the corresponding layerviewer
 *
 * @return std::map<float, LayerViewer *>
 */
std::map<float, LayerViewer*> LayerGroup::getInteractionFields() const
{
    return interactionFields;
}

/**
 * @brief set the layerviewer map
 *
 * @param std::map<float
 * @param value
 */
void LayerGroup::setInteractionFields(const std::map<float, LayerViewer*> &value)
{
    interactionFields = value;
}

/**
 * @brief add a layerViewer to the layerviewer map
 *   if the parameter is already used, override the parameter value
 * @param parameter parameter at which we place the new layer viewer
 * @param layer layerviewer to add to the map
 */
void LayerGroup::addInteractionFields(float parameter, LayerViewer *layer)
{
    interactionFields[parameter] = layer;

}

/**
 * @brief interpolate between the layerviewers' matrices to obtain the current layerviewer's matrix
 *
 * @param parameter the parameter where we are to interpolate
 * @return LayerViewer the resulting layerviewer
 */
LayerViewer LayerGroup::computeLayerAt(float parameter) const
{

        // find the matrix to use, or the two matrices to interpolate between
        const LayerViewer *matrix0 = nullptr, *matrix1 = nullptr;
        float mu;
        getLayersForInterpolation(parameter, matrix0, matrix1, mu);

        // if there is only one matrix to use, visualize it in a straight-forward way
        if (matrix1 == nullptr)
            return *matrix0; //change for control vectors

        // otherwise, draw an "interpolated matrix" that lies between matrix0 and matrix1.
        // The grid rectangle is an interpolation of matrix0 and matrix1, which may have different sizes.
        // We cannot draw one of these matrices directly. Instead, we should create a new grid and fill it with interpolated values.

        // - the rotation is interpolated between matrix0 and matrix1
        //float angle0 = getRotationAngleFromLinks(matrix0, source, linkObjects, world);
        //float angle1 = getRotationAngleFromLinks(matrix1, source, linkObjects, world);
        //float angle = (1 - mu)*angle0 + mu * angle1;

        // - the grid's origin is interpolated as well


        const QPointF   & layerBorderCenter = (1 - mu)*matrix0->getBorder().center()+ mu * matrix1->getBorder().center();
        //const QPointF& currentSourcePosition = ;
        //const Vector2D& translation = currentSourcePosition - sourcePosition;

        // - the grid's width/height is interpolated as well
        float w = (1 - mu)*matrix0->getBorder().width() + mu * matrix1->getBorder().width();
        float h = (1 - mu)*matrix0->getBorder().height() + mu * matrix1->getBorder().height();
        int cols = (int)round((1 - mu)*matrix0->getMatrix()->getCols() + mu * matrix1->getMatrix()->getCols());
        int rows = (int)round((1 - mu)*matrix0->getMatrix()->getRows() + mu * matrix1->getMatrix()->getRows());
        float cellWidth = w / cols;
        float cellHeight = h / rows;

        std::vector<Vector2D> vectorPos0;
        std::vector<Vector2D> vectorPos1;
        std::vector<Vector2D> vectors0;
        std::vector<Vector2D> vectors1;
        for(auto &line :matrix0->getControlVectors()){
            vectorPos0.push_back(Vector2D(line->getStartPoint().x(), line->getStartPoint().y()));
            vectors0.push_back((float)line->getCoeffColor()/100.0f*Vector2D((line->getEndPoint()-line->getStartPoint()).x(),(line->getEndPoint()-line->getStartPoint()).y()).getnormalized());

        }
        for(auto &line :matrix1->getControlVectors()){
            vectorPos1.push_back(Vector2D(line->getStartPoint().x(), line->getStartPoint().y()));
            vectors1.push_back((float)line->getCoeffColor()/100.0f*Vector2D((line->getEndPoint()-line->getStartPoint()).x(),(line->getEndPoint()-line->getStartPoint()).y()).getnormalized());

        }
        // fill in all grid points
        VectorMatrix *currentMatrix = new VectorMatrix(rows,cols);
        for (int i = 0; i < rows; ++i)
        {
            for (int j = 0; j < cols; ++j)
            {
                const Vector2D basePosition((j+0.5f) * cellWidth, (rows-i-0.5f) * cellHeight);
                //const Vector2D& rotatedPosition = rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
                //const Vector2D& worldPosition = rotatedPosition + translation;

                // compute the interpolated IF vector at this position
                const Vector2D& IFVector =
                    (1 - mu)*currentMatrix->interpolationInverseDistanceEuclidianAtPosition(1.85, basePosition, vectorPos0, vectors0)
                    + mu * currentMatrix->interpolationInverseDistanceEuclidianAtPosition(1.85, basePosition, vectorPos1, vectors1);

                currentMatrix->setAt(i,j,IFVector);
            }
        }
        LayerViewer currentLayer(w, h, currentMatrix);
        currentLayer.setBorder(QRectF(layerBorderCenter-QPointF(w/2., h/2.) ,QSize(w,h)));

        return currentLayer;
}



/**
 * @brief save the grouplayer
 * for each layer viewer create a txt file and writte the matrix or the list of control curves
 * @param fileName name of the file can be his name or one chose by the user
 * @param fileFormat
 * @param isContinuus: if true => the file is saved as a list of guide curve and the interpolation is done during the simulation (for VR), if false, the entire gride is saved (for 2D simulation on UMANS).
 * @return bool
 */
bool LayerGroup::saveGroup(const QString &fileName, const char *fileFormat, bool isContinuus )
{
    bool result;
    if(interactionFields.size()>1){
        for(auto &layer : interactionFields)
        {
            float key = layer.first;
            const QString name = fileName + "_"+QString::number(key) +"."+fileFormat;
            if(isContinuus){
                result = layer.second->saveImageQuick(name, setMatrixSource());//change to "saveImage()" for old format txt
            }
            else{
                 result = layer.second->saveImage(name, setMatrixSource());
            }

        }
    }
    else{

        auto layer = interactionFields[0];
        const QString name = fileName + "."+fileFormat;
        if(isContinuus){
            result = layer->saveImageQuick(name, setMatrixSource());//change to "saveImage()" for old format txt

        }
        else{
            result = layer->saveImage(name, setMatrixSource());
        }

    }

    return result;
}



/**
 * @brief return the current layerviewer been edited in the renderArea
 *
 * @return LayerViewer
 */
LayerViewer* LayerGroup::getCurrentLayer() const
{
    LayerViewer *result;
     if(isParametric){

         if(!interactionFields.count(parameter)){
             result = new LayerViewer(computeLayerAt(getParameter()));
         }
         else
            result = getInteractionFields().at(parameter);
     }
     else
         result = getInteractionFields().at(0.0f);
     return result;
}



/**
 * @brief check if one of the layerviewer of the grouplayer has been modified
 *
 * @return bool
 */
bool LayerGroup::isModified() const
{

    for(auto& layer:interactionFields)
        if (layer.second->getIsModified()){
            return true;
        }
    return false;
}


/**
 * @brief find the matrices (one or two) to used to interpolate with this parameter value
 *
 * @param parameterValue the parameter value we wish to interpolate for
 * @param result0 the layerViewer with the closest inferior parameter value
 * @param result1 the layerViewer with the closest superior parameter value
 * @param result_mu the weight to use to interpolate bewteen the two matrices
 */
void LayerGroup::getLayersForInterpolation(const float parameterValue, const LayerViewer*& result0, const LayerViewer*& result1, float& result_mu) const
{


    // find the matrix (or two matrices) to use for this parameter value
    for (std::map<float, LayerViewer*>::const_iterator it = interactionFields.begin(); it != interactionFields.end(); ++it)
    {
        float paramMax = it->first;
        if (parameterValue <= paramMax)
        {
            // check if we should interpolate between this matrix and the previous
            if (parameterValue < paramMax && it !=  interactionFields.begin())
            {
                const auto& prev = std::prev(it);
                float paramMin = prev->first;
                result0 = prev->second;
                result1 = it->second;
                result_mu = (parameterValue - paramMin) / (paramMax - paramMin);
            }

            // if there's no interpolation to be done, use only this matrix
            else
            {
                result0 = it->second;
                result1 = nullptr;
                result_mu = 0;
            }

            return;
        }
    }

    // parameter is larger than the max; just use the last matrix
    result0 = interactionFields.rbegin()->second;
    result1 = nullptr;
    result_mu = 0;
}

/**
 * @brief
 *
 * @return float
 */
float LayerGroup::getParameter() const
{
    return parameter;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setParameter(float value)
{
    parameter = value;
    borderRect = QRectF(getCurrentLayer()->getBorder());

}

/**
 * @brief
 *
 * @return bool
 */
bool LayerGroup::getIsParametric() const
{
    return isParametric;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setIsParametric(bool value)
{
    isParametric=value;
}

/**
 * @brief the border of the layergroup
 *
 * @return QRectF
 */
QRectF LayerGroup::boundingRect() const
{
    return QRectF(borderRect.center()-QPointF(getCurrentLayer()->getWidth()/2., getCurrentLayer()->getHeight()/2.),QSize(getCurrentLayer()->getWidth(), getCurrentLayer()->getHeight()));
}

/**
 * @brief
 *
 * @return QString
 */
QString LayerGroup::getName() const
{
    return name;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setName(const QString &value)
{
    name = value;
}

/**
 * @brief check if the layergroup has a source
 *
 * @return bool
 */
bool LayerGroup::getHasSource() const
{
    for(auto &pole:getPoles()){
        if(pole->getIsSource())
            return true;
    }
    return false;
}
/**
 * @brief
 *
 * @return bool
 */
bool LayerGroup::getIsHidden() const
{
    return isHidden;
}

/**
 * @brief
 *
 * @param value
 */
void LayerGroup::setIsHidden(bool value)
{
    isHidden = value;
}



/*
float LayerViewer::getRotationAngleFromLinks(const LayerGroup* layer, const IFSource* source) const
{
    // TODO: How to combine multiple links? Right now we use only the first link to compute a rotation angle.

    const Vector2D unitVectorDown(0, -1);
    const Vector2D& sourcePosition = source->getPosition();

    size_t nrLinks = std::min(layer->getLinks().size(), linkObjects.size());
    for (size_t i = 0; i < nrLinks; ++i)
    {
        const Link& link = *layer->getLinks()[i];
        const IFSourceReference& linkObjectRef = linkObjects[i];
        const IFSource* linkObject = nullptr;
        if (linkObjectRef.objectType == IFSourceType::AgentSource)
            linkObject = world->GetAgent_const(linkObjectRef.objectID);
        else
            linkObject = world->GetObstacle_const(linkObjectRef.objectID);

        // if this link is an angular relation with another object, use the current angle with that object
        if (linkObject != nullptr && link.getType() == Link::LinkType::Rotation)
            return link.getRotationAngle(source->getPosition(), linkObject->getPosition());
    }

    // otherwise, use the viewing angle that the source currently has
    return clockwiseAngle(unitVectorDown, source->getViewingDirection());
}*/

