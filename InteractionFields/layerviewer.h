#ifndef LAYERVIEWER_H
#define LAYERVIEWER_H

#include <QWidget>
#include "vectorcurve.h"
#include "vectormatrix.h"
#include "vectorline.h"
#include "layergroup.h"
#include "pole.h"
#include <QGraphicsItemGroup>
#include <QPointF>
#include <QWidget>
#include <QCursor>
#include <QPainterPath>
#include <vector>
#include <array>
#include <QSlider>
#include <QProgressBar>

class VectorMatrix;
class VectorCurve;
class VectorLine;

class LayerViewer : public QGraphicsItemGroup
{
public:
    LayerViewer();


    LayerViewer(float width_, float height_, VectorMatrix *matrix, QGraphicsItem *parent = nullptr);
    LayerViewer(float width_, float height_, QGraphicsItem *parent = nullptr);
    LayerViewer(const LayerViewer &layer);

    void gridShow();
    void gridCreate();
    void includeControlVector();
    void generateMatrix(float magnitude);
    void drawMatrix();
    double getMyPenWidth() const;
    void setMyPenWidth(double value);
    void hideCurves();
    void showCurves();
    void setMatrixSize(int newHeight, int newWidth);
    double getHeight() const;
    void setHeight(double value);

    double getWidth() const;
    void setWidth(double value);

    std::string setMatrixSource();



    QList<VectorLine *> getControlVectors() const;
    void setControlVectors(const QList<VectorLine *> &value);
    void generateMatrixFomFile(QString &fileName);
    std::array<int, 2> getCase(QPointF point);
    bool saveImage(const QString &fileName, const std::string &fileFormat);
    void setVectCurves(const QList<VectorCurve *> &value);
    QList<VectorCurve *> getVectCurves() const;
    void removeVectorCurve(VectorCurve *curve);
    VectorMatrix *getMatrix() const;
    void setMatrix(VectorMatrix *value);
    const Vector2D getSourcePosition() const;
    QRectF getBorder() const;
    void setBorder(const QRectF &value);
    float getValue();
    QRectF boundingRect() const override;
    void removeVectorLine(VectorCurve *line);
    bool saveImageQuick(const QString &fileName, const std::string &fileFormat);
    std::vector<std::array<int,2> > getZerosArea() const;
    void setZerosArea(const std::vector<std::array<int, 2> > &value);
    void addZerosArea(const std::array<int,2> ind);

    void addVectorCurve(VectorCurve *curve);
    //QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    bool getIsModified() const;
    void setIsModified(bool value);

    void clearLayer();
    void updateLink(Link *link);

    void removeZeroArea();

    void removeZero(std::array<int, 2> zero);
protected:


private:

    QList<VectorCurve *> vectCurves;
    QList<VectorLine *> controlVectors;
    VectorMatrix *matrix;
    double height;
    double width;
    bool isModified = false;
    QRectF border;
    double myPenWidth;

    std::vector<std::array<int,2>> zerosArea;



};

#endif // LAYERVIEWER_H
