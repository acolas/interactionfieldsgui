#ifndef POLE_H
#define POLE_H

#include <QGraphicsPixmapItem>
#include <QVector>
#include <QObject>
#include <QWidget>
#include <vectorline.h>
#include <link.h>
#include <vector2d.h>

QT_BEGIN_NAMESPACE
class QPixmap;
class QGraphicsSceneContextMenuEvent;
class QMenu;
class QPolygonF;
class VectorLine;
class Link;
QT_END_NAMESPACE


class Pole: public QGraphicsPolygonItem
{
public:

    Pole();


    enum { Type = UserType + 5 };
    enum Pole_Type { Agent, Obstacle_square, StartEnd, Io };

    Pole(Pole_Type poleType, QMenu *contextMenu, bool s, QGraphicsItem *parent = nullptr);

    void removeLink(Link *link);
    void removeLinks();
    Pole_Type PoleType() const { return myPoleType; }
    QPolygonF polygon() const { return myPolygon; }
    void addLink(Link *link);
    QPixmap image() const;
    int type() const override { return Type; }

    bool getIsSource() const;
    void setIsSource(bool value);

    QVector<Link *> getLinks() const;

    size_t getId() const;
    void setId(size_t value);

    std::string getTypeString() const;
    void changeSize(float h, float w);


    double getHeight() const;
    void setHeight(double value);

    double getWidth() const;
    void setWidth(double value);


protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

private:
    Pole_Type myPoleType;
    bool isSource;
    QPolygonF myPolygon;
    QMenu *myContextMenu;
    QVector<Link *> links;
    size_t id;
    std::string typeString;
    double height;
    double width;

};

#endif // POLE_H

