
#include <cmath>
#include <vector>
#include <iostream>
#include <array>
#include <algorithm>

#include "vectormatrix.h"

using namespace std;

/**
 * @brief contructor of a vector matrix
 *initialize avec vectors
 * @param rows number of rows
 * @param cols number of columns
 */
VectorMatrix::VectorMatrix(int rows, int cols) : rows_(rows), cols_(cols)
{
    allocSpace();
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = Vector2D();
        }
    }
}

/**
 * @brief contructor
 * initialise to a 1x1 matrix
 */
VectorMatrix::VectorMatrix() : rows_(1), cols_(1)
{
    allocSpace();
    mat[0][0] = Vector2D();
}

/**
 * @brief delete Vectormatrix
 *delete each array of the matrix
 */
VectorMatrix::~VectorMatrix()
{
    for (int i = 0; i < rows_; ++i) {
        delete[] mat[i];
    }
    delete[] mat;
}

/**
 * @brief set a vector to indices of rows and columns
 *
 * @param i index of the rows
 * @param j index of the columns
 * @param vect vector to place in the matrix
 */
void VectorMatrix::setAt(int i, int j,const Vector2D &vect)
{
    if (i>=rows_ || i<0 || j>=cols_ || j<0 ){

        throw std::out_of_range("Out of range value set VectorMatrix::setAt");
    }
    else{
        mat[i][j] = vect;
    }

}

/**
 * @brief read the vector placed at certains indices
 *
 * @param r index r of the rows
 * @param c index c of the column
 * @return const Vector2D the retreived vector
 */
const Vector2D &VectorMatrix::at(int r, int c) const
{
    if (r>=rows_ || r<0 || c>=cols_ || c<0 ){
        throw std::out_of_range("Out of range value set VectorMatrix::at" +  std::to_string(r)+" "+ std::to_string(rows_)+" " +  std::to_string(c)+" "+std::to_string(cols_));
    }
    else{
         return mat[r][c];
    }

    }
/*
VectorMatrix::VectorMatrix(const VectorMatrix& m) : rows_(m.rows_), cols_(m.cols_)
{
    allocSpace();
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = m.mat[i][j];

        }
    }
}

VectorMatrix& VectorMatrix::operator=(const VectorMatrix& m)
{
    if (this == &m) {
        return *this;
    }

    if (rows_ != m.rows_ || cols_ != m.cols_) {
        for (int i = 0; i < rows_; ++i) {
            delete[] mat[i];
        }
        delete[] mat;

        rows_ = m.rows_;
        cols_ = m.cols_;
        allocSpace();
    }

    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = m.mat[i][j];
        }
    }
    return *this;
}


*/

VectorMatrix& VectorMatrix::operator+=(const VectorMatrix& m)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] += m.mat[i][j];
        }
    }
    return *this;
}

/**
 * @brief
 *
 * @param m
 * @return VectorMatrix &VectorMatrix::operator
 */
VectorMatrix& VectorMatrix::operator-=(const VectorMatrix& m)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] -= m.mat[i][j];
        }
    }
    return *this;
}



/**
 * @brief
 *
 * @param num
 * @return VectorMatrix &VectorMatrix::operator
 */
VectorMatrix& VectorMatrix::operator*=(float num)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = mat[i][j]*num;
        }
    }
    return *this;
}

/**
 * @brief
 *
 * @param num
 * @return VectorMatrix &VectorMatrix::operator
 */
VectorMatrix& VectorMatrix::operator/=(float num)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = mat[i][j]/num;
        }
    }
    return *this;
}

// Access the individual elements

//Vector2D& VectorMatrix::operator()(const unsigned& row, const unsigned& col) {
//  return this->mat[row][col];
//}

//// Access the individual elements (const)

//const Vector2D& VectorMatrix::operator()(const unsigned& row, const unsigned& col) const {
//  return this->mat[row][col];
//}

/**
 * @brief swap 2 rows r1 and r2: never used
 *
 * @param r1
 * @param r2
 */
void VectorMatrix::swapRows(int r1, int r2)
{
    Vector2D *temp = mat[r1];
    mat[r1] = mat[r2];
    mat[r2] = temp;
}

/**
 * @brief trasnpose matrix: never used
 *
 * @return VectorMatrix
 */
VectorMatrix VectorMatrix::transpose()
{
    VectorMatrix ret(cols_, rows_);
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            ret.mat[j][i] = mat[i][j];
        }
    }
    return ret;
}



/**
 * @brief
 *
 * @param a
 * @param b
 * @return double
 */
double VectorMatrix::dotProduct( const VectorMatrix &a,const  VectorMatrix &b)
{
    double sum = 0;
    for (int i = 0; i < a.rows_; ++i) {
        sum += (a.at(i, 0).dot(b.at(i, 0)));
    }
    return sum;
}




/**
 * @brief
 *
 * @return int
 */
int VectorMatrix::getRows() const
{
    return rows_;
}

/**
 * @brief
 *
 * @return int
 */
int VectorMatrix::getCols() const
{
    return cols_;
}

/**
 * @brief don't think this is necessay, to be removed! CHECK
 *
 * @return std::array<int, _Tp2>
 */
std::array<int, 2> VectorMatrix::getSource() const
{
    return source;
}

/**
 * @brief
 *
 * @param std::array<int
 * @param value
 */
void VectorMatrix::setSource(const std::array<int,2> &value)
{
    source = value;
}

/**
 * @brief
 *
 * @return std::vector<std::array<int, _Tp2> >
 */
std::vector<std::array<int, 3> > *VectorMatrix::getPoles() const
{
    return poles;
}

/**
 * @brief
 *
 * @param std::vector<std::array<int
 * @param value
 */
void VectorMatrix::setPoles(std::vector<std::array<int, 3> > *value)
{
    poles = value;
}

//void VectorMatrix::setRows(int rows)
//{
//    rows_ = rows;
//}

//void VectorMatrix::setCols(int cols)
//{
//    cols_ = cols;
//}




/**
 * @brief
 *
 * @return const std::vector<std::array<int, _Tp2> >
 */
const std::vector<std::array<int,2>> & VectorMatrix::getControlPoints() const
{
    return controlPoints;
}

/**
 * @brief
 *
 * @param std::vector<std::array<int
 * @param value
 */
void VectorMatrix::setControlPoints(const std::vector<std::array<int,2>> &value)
{
    controlPoints.clear();
    controlPoints = value;
}

/**
 * @brief
 *
 * @param a
 * @param b
 */
void VectorMatrix::addControlPoints(const int &a, const int &b)
{
    array<int, 2> index {a,b};

    controlPoints.push_back(index);
}
//WRONG
/**
 * @brief
 *
 */
void VectorMatrix::findControlPoints()
{

    for (int i = 0; i < rows_; i++) {
        for (int j = 0; j < cols_; j++) {
            if (mat[i][j].isZero()==false) {
                addControlPoints(i, j);
            }
        }
    }
}

/**
 * @brief
 *
 * @return bool
 */
bool VectorMatrix::isNul()
{
    for (int i = 0; i < rows_; i++) {
        for (int j = 0; j < cols_; j++) {
            if (mat[i][j].isZero()==false) {
                return false;
               
            }
        }
    }
    return true;
}



/**
 * @brief simple interpolation test but not the chosen one
 * just add all the vector divided by the manhattan distance betwenn the vector indices and the current indices
 */
void VectorMatrix::interpolationSimple()
{
    findControlPoints();
    for( int i =0; i< rows_; i++){
        for (int j =0; j<cols_; j++){
            array<int,2> arr {i,j};
            if(find(controlPoints.begin(), controlPoints.end(), arr) == controlPoints.end()){
                Vector2D sum(0,0);

                for(const array<int, 2> &ind : controlPoints ){

                    sum = sum + mat[ind[0]][ind[1]]/manhantanLength(arr,ind);
                }
                setAt(i,j, sum);
            }
        }
    }
}

/**
 * @brief simple interpolation inverse distance, here we use the inverse distance as weight but with manhattan distance again => not the most efficient
 *
 * @param p paramter for the interpolation
 */
void VectorMatrix::interpolationInverseDistanceManhattan(float p)
{

    if (p < 0 || p>2) {

        throw std::out_of_range("Out of range value set VectorMatrix::interpolationInverseDistanceManhattan");
    }
    else if(controlPoints.empty()){
            setToNul();
    }
    else {
        for( int i =0; i< rows_; i++){
            for (int j = 0; j < cols_; j++) {
                array<int, 2> arr{ i,j };
                if (find(controlPoints.begin(), controlPoints.end(), arr) == controlPoints.end()) {
                    Vector2D num(0, 0);
                    float denom = 0.0;

                    for (const array<int, 2> &ind : controlPoints) {

                        float w = pow(1 / float(manhantanLength(arr, ind)), p);
                        denom += w;
                        //                        Vector2D vect = mat[ind[0]][ind[1]];

                        num = num + w * mat[ind[0]][ind[1]];
                    }

                    setAt(i, j, num / denom);
                }

            }
        }

    }
}
/**
 * @brief interpolation inverse distance with continuous distance:more accurate
 *
 * @param p parameter of the inverse distance
 * @param unitH_ height of one cell
 * @param unitW_ width of one cell
 * @param originPos position of the top left corner of the matrix in the scene
 * @param vecPos positions of the control vectors
 * @param controlVectors control vectors
 * @param magnitude desired magnitude for the obtained vectors
 */
void VectorMatrix::interpolationInverseDistanceEuclidian(float p, float unitH_, float unitW_, Vector2D originPos, std::vector<Vector2D> vecPos, std::vector<Vector2D> controlVectors, float magnitude)
{


    if (p < 0 || p>2) {

        throw std::out_of_range("Out of range value set VectorMatrix::interpolationInverseDistanceEuclidian");
    }
    else if(controlVectors.empty()){

         setToNul();
    }
    else {
        originPos = originPos + Vector2D(unitW_/2.0f, -unitH_/2.0f);
        for( int i =0; i< rows_; i++){
            for (int j = 0; j < cols_; j++) {


                    Vector2D pos = Vector2D(originPos.x() + j * unitW_, originPos.y() - i * unitH_);
                    Vector2D num(0, 0);
                    float denom = 0.0;

                    for (int k =0; k<controlVectors.size(); k++) {

                        Vector2D vectorControl = controlVectors[k];
                        Vector2D vectControlPosition = vecPos[k];



                        float w = pow(1 / float((vectControlPosition- pos).magnitude()), p);
                        denom += w;
                        //                        Vector2D vect = mat[ind[0]][ind[1]];

                        num = num + w * vectorControl;
                    }
                    Vector2D res = (num/denom).getnormalized()*magnitude;
                    setAt(i, j, res);




            }
        }

    }
}
/**
 * @brief same than previously but the magntiude of the vectors are commputed according to the control vectors.
 *
 * @param p
 * @param unitH_
 * @param unitW_
 * @param originPos
 * @param vecPos
 * @param controlVectors
 */
void VectorMatrix::interpolationInverseDistanceEuclidian(float p, float unitH_, float unitW_, Vector2D originPos, std::vector<Vector2D> vecPos, std::vector<Vector2D> controlVectors)
{


    if (p < 0 || p>2) {

        throw std::out_of_range("Out of range value set VectorMatrix::interpolationInverseDistanceEuclidian");
    }
    else if(controlVectors.empty()){

         setToNul();
    }
    else {
        originPos = originPos + Vector2D(unitW_/2.0f, -unitH_/2.0f);
        for( int i =0; i< rows_; i++){
            for (int j = 0; j < cols_; j++) {


                    Vector2D pos = Vector2D(originPos.x() + j * unitW_, originPos.y() - i * unitH_);
                    Vector2D num(0, 0);
                    float denom = 0.0;

                    for (int k =0; k<controlVectors.size(); k++) {

                        Vector2D vectorControl = controlVectors[k];
                        Vector2D vectControlPosition = vecPos[k];



                        float w = pow(1 / float((vectControlPosition- pos).magnitude()), p);
                        denom += w;
                        //                        Vector2D vect = mat[ind[0]][ind[1]];

                        num = num + w * vectorControl;
                    }
                    Vector2D res = (num/denom);
                    setAt(i, j, res);




            }
        }

    }
}
/**
 * @brief get the vector at a certain position after Inversedistance itnerpolation interpolation: not used
 * the magnitude is chosen by the control vectors
 * @param p parameter of the interpolation
 * @param position position of the vector
 * @param vectPos positons of the control vectors
 * @param vectControl control vectors
 * @return const Vector2D value pf the final vector
 */
const Vector2D VectorMatrix::interpolationInverseDistanceEuclidianAtPosition(float p, Vector2D position, std::vector<Vector2D> vectPos, std::vector<Vector2D> vectControl)
{
    Vector2D num(0, 0);
    float denom = 0.0;
    for (int k =0; k<vectControl.size(); k++) {

        Vector2D vectorControl = vectControl[k];
        Vector2D vectControlPosition = vectPos[k];

        float mag = float(vectorControl.magnitude());


        float w = pow(1 / float((vectControlPosition- position).magnitude()), p);
        denom += w;
        //                        Vector2D vect = mat[ind[0]][ind[1]];

        num = num + w * vectorControl;
    }
    return num/denom;

}

/**
 * @brief same with given magnitude
 *
 * @param float
 * @param position
 * @param vectPos
 * @param vectControl
 * @param magnitude
 * @return const Vector2D
 */
const Vector2D VectorMatrix::interpolationInverseDistanceEuclidianAtPosition(float p, Vector2D position, std::vector<Vector2D> vectPos, std::vector<Vector2D> vectControl, float magnitude)
{
    Vector2D num(0, 0);
    float denom = 0.0;
    for (int k =0; k<vectControl.size(); k++) {

        Vector2D vectorControl = vectControl[k];
        Vector2D vectControlPosition = vectPos[k];

        float mag = float(vectorControl.magnitude());


        float w = pow(1 / float((vectControlPosition- position).magnitude()), p);
        denom += w;
        //                        Vector2D vect = mat[ind[0]][ind[1]];

        num = num + w * vectorControl;
    }
    return (num/denom).getnormalized()*magnitude;

}





/**
 * @brief compute manhantanLength of 2 indices
 *
 * @param array<int
 * @param A
 * @param array<int
 * @param B
 * @return int
 */
int VectorMatrix::manhantanLength(const array<int, 2> &A, const  array<int, 2> &B)
{
    return abs(A[0]-B[0])+ abs(A[1]-B[1]);
}


/**
 * @brief
 *
 */
void VectorMatrix::clear(){
    try {
        allocSpace();
    } catch (const std::exception& e) {
        std::cout << e.what();
    }


}

/**
 * @brief
 *
 */
void VectorMatrix::setToNul()
{

    for(int i = 0; i<rows_; i++){
        for(int j =0; j<cols_;j++){
            setAt(i,j, Vector2D(0,0));
        }
    }
}
                /* PRIVATE HELPER FUNCTIONS
 ********************************/

void VectorMatrix::allocSpace()
{
mat = new Vector2D*[rows_];
for (int i = 0; i < rows_; ++i) {
    mat[i] = new Vector2D[cols_];
    }
}



/* NON-MEMBER FUNCTIONS
 ********************************/

VectorMatrix operator+(const VectorMatrix& m1, const VectorMatrix& m2)
{
    VectorMatrix temp(m1);
    return (temp += m2);
}

/**
 * @brief
 *
 * @param m1
 * @param m2
 * @return VectorMatrix operator
 */
VectorMatrix operator-(const VectorMatrix& m1, const VectorMatrix& m2)
{
    VectorMatrix temp(m1);
    return (temp -= m2);
}



/**
 * @brief
 *
 * @param m
 * @param num
 * @return VectorMatrix operator
 */
VectorMatrix operator*(const VectorMatrix& m, float num)
{
    VectorMatrix temp(m);
    return (temp *= num);
}



/**
 * @brief
 *
 * @param m
 * @param num
 * @return VectorMatrix operator
 */
VectorMatrix operator/(const VectorMatrix& m, float num)
{
    VectorMatrix temp(m);
    return (temp /= num);
}
