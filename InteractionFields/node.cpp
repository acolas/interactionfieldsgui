
#include "node.h"
#include "renderarea.h"
#include "vectorline.h"
#include "vectorcurve.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>

//! [0]

/**
 * @brief constructor
 *
 * @param parent
 */
Node::Node(QGraphicsItem *parent)
{
    setFlag(ItemIsMovable, true);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(1);
}

/**
 * @brief
 *
 * @param p
 * @param parent
 */
Node::Node(QPointF p, QGraphicsItem *parent)
{
    setPos(p);
    setFlag(ItemIsMovable, true);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(1);
}
//! [0]


//! [2]


//! [7]
/**
 * @brief
 *
 * @return bool
 */
bool Node::advancePosition()
{
    if (newPos == pos())
        return false;

    setPos(newPos);
    return true;
}
//! [7]

//! [8]
/**
 * @brief the bounding rect, used to select the node
 *
 * @return QRectF
 */
QRectF Node::boundingRect() const
{
    qreal adjust = 0.2*5.*curve->getWidth();
    /*
    QGraphicsEllipseItem* el = new QGraphicsEllipseItem(pos().x(),pos().y(), 6, 6);

    return el->boundingRect();*/
    return QRectF( -0.5*5.*curve->getWidth() - adjust, -0.5*5.*curve->getWidth() - adjust, 1*5.*curve->getWidth()+ 2*adjust, 1*5.*curve->getWidth() + 2*adjust);

}
//! [8]

//! [9]
/**
 * @brief
 *
 * @return QPainterPath
 */
QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-0.5* 5.*curve->getWidth(), -0.5*5.*curve->getWidth(), 5.*curve->getWidth(), 5.*curve->getWidth());
    return path;
}
//! [9]

//! [10]
/**
 * @brief design the node to paint it on the render area scene
 *
 * @param painter
 * @param option
 * @param
 */
void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::darkGray);
    painter->drawEllipse(QRectF(-0.5*5.*curve->getWidth(), -0.5*5.*curve->getWidth(), 5.*curve->getWidth(), 5.*curve->getWidth()));

    QRadialGradient gradient(-0.3, -0.3, 1);
    if (option->state & QStyle::State_Sunken) {
        gradient.setCenter(3, 3);
        gradient.setFocalPoint(3, 3);
        gradient.setColorAt(1, QColor(Qt::yellow).lighter(120));
        gradient.setColorAt(0, QColor(Qt::darkYellow).lighter(120));
    } else {
        gradient.setColorAt(0, Qt::yellow);
        gradient.setColorAt(1, Qt::darkYellow);
    }
    painter->setBrush(gradient);

    painter->setPen(QPen(Qt::black, 0));
    painter->drawEllipse(QRectF(-0.5* 5.*curve->getWidth(), -0.5*5.*curve->getWidth(), 5.*curve->getWidth(), 5.*curve->getWidth()));
}
//! [10]

//! [11]
/**
 * @brief
 * when the node is displaced , change the control curve
 * @param change
 * @param value
 * @return QVariant
 */
QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {

    case ItemPositionHasChanged:

        if ((curve !=nullptr))
            curve->changePointLine(index, pos());
        //else if (line != nullptr)
            //line->change
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}
//! [11]

//! [12]
/**
 * @brief
 *
 * @param event
 */
void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

/**
 * @brief
 *
 * @param event
 */
void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

/**
 * @brief
 *
 * @return bool
 */
bool Node::getManualChange() const
{
    return manualChange;
}

/**
 * @brief
 *
 * @param value
 */
void Node::setManualChange(bool value)
{
    manualChange = value;
}



/**
 * @brief
 *
 * @param pos
 */
void Node::updatePosition(QPointF pos)
{
    this->setPos(pos);
}


/**
 * @brief
 *
 * @return VectorCurve
 */
VectorCurve *Node::getCurve() const
{
    return curve;
}

/**
 * @brief
 *
 * @param value
 */
void Node::setCurve(VectorCurve *value)
{
    curve = value;
}

/**
 * @brief
 *
 * @return int
 */
int Node::getIndex() const
{
    return index;
}

/**
 * @brief
 *
 * @param value
 */
void Node::setIndex(int value)
{
    index = value;
}
//! [12]
