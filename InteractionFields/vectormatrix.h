#ifndef VECTORMATRIX_H
#define VECTORMATRIX_H

#include <vector>
#include <array>
#include "vector2d.h"
#include <iostream>
#include <QPointF>
#include <QList>

class VectorMatrix
{
public:
    VectorMatrix(int init_row, int init_column);

    //VectorMatrix(const VectorMatrix &mat);
    VectorMatrix();
    ~VectorMatrix();

    void setAt(int i, int j,const  Vector2D &);

    //VectorMatrix& operator =(const VectorMatrix&);


    //inline Vector2D& operator()(int x, int y) { return mat[x][y]; }

    const Vector2D& at(int r, int c) const;

    VectorMatrix& operator+=(const VectorMatrix&);
    VectorMatrix& operator-=(const VectorMatrix&);
    VectorMatrix& operator*=(float);
    VectorMatrix& operator/=(float);


    //    Vector2D& operator()(const unsigned& row, const unsigned& col);
    //    const Vector2D& operator()(const unsigned& row, const unsigned& col)const;

    void swapRows(int, int);
    VectorMatrix transpose();



    // functions on vectors
    static double dotProduct(const VectorMatrix &,const VectorMatrix &);


    void interpolationSimple();
    void interpolationInverseDistanceManhattan(float p);
    void interpolationInverseDistanceEuclidian(float p,float unitH_, float unitW_, Vector2D originPos, std::vector<Vector2D> vectPos, std::vector<Vector2D> vectControl);
    void interpolationInverseDistanceEuclidian(float p,float unitH_, float unitW_, Vector2D originPos, std::vector<Vector2D> vectPos, std::vector<Vector2D> vectControl, float magnitude);
    const Vector2D interpolationInverseDistanceEuclidianAtPosition(float,Vector2D position, std::vector<Vector2D> vectPos, std::vector<Vector2D> vectControl);
    const Vector2D interpolationInverseDistanceEuclidianAtPosition(float,Vector2D position, std::vector<Vector2D> vectPos, std::vector<Vector2D> vectControl, float magnitude);
    int manhantanLength(const std::array<int, 2> &,const std::array<int, 2> &);
    void findControlPoints();
    bool isNul();
    void clear();
    void setToNul();


    const  std::vector<std::array<int, 2>> & getControlPoints() const;
    void setControlPoints(const std::vector<std::array<int, 2>> &value);
    void addControlPoints(const int &a, const int &b);

    int getRows() const;

    int getCols() const;

    std::array<int, 2> getSource() const;

    void setSource(const std::array<int,2> &value);




//    void setRows(int rows);

//    void setCols(int cols);

    std::vector<std::array<int, 3> > *getPoles() const;
    void setPoles(std::vector<std::array<int, 3> > *value);

private:
    int rows_;
    int cols_;
    std::array<int,2> source {-1,-1};
    Vector2D **mat;
    std::vector<std::array<int, 2>> controlPoints; ;
    std::vector<std::array<int,3>>* poles;
    std::string tag;

    void allocSpace();

};


VectorMatrix operator+(const VectorMatrix&, const VectorMatrix&);
VectorMatrix operator-(const VectorMatrix&, const VectorMatrix&);
VectorMatrix operator*(const VectorMatrix&, float);
//VectorMatrix operator*(float, const VectorMatrix&);
VectorMatrix operator/(const VectorMatrix&, float);


#endif // VECTORMATRIX_H
