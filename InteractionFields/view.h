#ifndef VIEW_H
#define VIEW_H

#include <QMainWindow>
#include <QList>
#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainterPath>
#include <QLineEdit>
#include <QBoxLayout>
#include <QStackedWidget>
#include <QStackedLayout>
#include <QGroupBox>
#include "pole.h"


class RenderArea;


QT_BEGIN_NAMESPACE
class QAction;
class QToolBox;
class QSpinBox;
class QComboBox;
class QFontComboBox;
class QButtonGroup;
class QLineEdit;
class QGraphicsTextItem;
class QFont;
class QToolButton;
class QAbstractButton;
class QGraphicsView;
class QSlider;
class QLabel;
namespace Ui { class View; }
QT_END_NAMESPACE


class View : public QMainWindow
{
    Q_OBJECT

public:
    View(QWidget *parent = nullptr);
    ~View();

    QWidget *createMatrixCellWidget(int id);
    void on_paramButton_clilcked();


protected:
    void closeEvent(QCloseEvent *event) override;
    void resizeEvent(QResizeEvent* event) override;



private slots:
    void open();
    void save(bool isExpended);
    void penColor();
    void penWidth();
    void matrixSize(size_t id);
    void sceneSize();
    void addLayer();
    void colorMagnitude();
    void about();
    void on_actionLine_triggered();
    void on_actionCurve_triggered();
    void on_actionAgent_triggered();
    void on_actionPole_triggered();

    void on_pushButtonMatrix_clicked();
    void on_agentRadButt_clicked();

    void on_obstacleRadButt_clicked();

    void on_rotationButton_clicked();

    void on_translationButton_clicked();

    void on_scaleButton_clicked();
    void sliderValueChange();

    //void on_scaleButton_stateChanged(int arg1);

    void layerButtonGroupClicked(int id);
    void matrixButtonGroupClicked(int id);
    void buttonGroupClicked(int id);
    void buttonGroupLinkClicked(int id);
    void blankButtonGroupClicked(size_t id);
    void hideButtonGroupClicked();
    void clearButtonGroupClicked();
    void deleteItem();
    void pointerGroupClicked(int id);
    void bringToFront();
    void sendToBack();
    void itemInserted(Pole *item);
    void sceneScaleChanged(const QString &scale);
    void makeSource();
    void showLayer(size_t id);
    void changeSliderColor(QSlider *s);
    void showVector(size_t id);
    void showGrid(size_t id);
    void moveLayer(size_t id);
    void angularSymetry();
    void createCentralView();
    //void showContextMenu(const QPoint &pos);
    void saveAsGrid(bool);
    void saveAsCurves(bool);
    bool saveSimple();
private:
    Ui::View *ui;

    size_t idGroup = 0;

    void createActions();
    void createMenus();
    void createToolBox();
    void createSliderToolBox();
    void createToolbars();
    void createLayerbars();


    QWidget *createLayerCellWidget(const QString &text, const double height, const double width, const double weight, size_t id, RenderArea *render);
    QWidget *createCellWidget(const QString &text, Pole::Pole_Type type);

    bool maybeSave();
    bool saveFile(const QByteArray &fileFormat, bool isExtended);

    bool isContinuus;
    RenderArea *renderArea;
    QGraphicsView *view;


    QAction *exitAction;
    QAction *addAction;
    QAction *deleteAction;

    QAction *toFrontAction;
    QAction *sendBackAction;
    QAction *aboutAction;


    QAction *openAct;
    //QList<QAction *> saveAsActs;
    QAction *saveAsGridAct;
    QAction *saveAsCurveAct;
    QAction *saveAct;
    QAction *loadAsAct;
    QAction *exitAct;
    QAction *penColorAct;
    QAction *penWidthAct;
    QAction *matrixSizeAct;
    QAction *sceneSizeAct;
    QAction *addLayerAct;
    QAction *clearScreenAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QAction *poleAct;
    QAction *sourceAction;
    QAction *makeParametricAction;
    QAction *sourceAct;
    QAction *showLayerAct;
    QAction *showVectorAct;
    QAction *moveLayerAct;
    QAction *eraseNullAct;



    QMenu *saveAsMenu;
    QMenu *fileMenu;
    QMenu *optionMenu;
    QMenu *helpMenu;
    QMenu *toolMenu;
    QMenu *itemMenu;
    QMenu *layerMenu;
    QMenu *brushMenu;
    QMenu *eraserMenu;



    QToolBox *toolBox;
    QToolBox *sliderToolBox;

    QButtonGroup *buttonGroup;
    QButtonGroup *matrixButtonGroup;
    QButtonGroup *pointerTypeGroup;
    QButtonGroup *linkTypeGroup;
    QButtonGroup *layerButtonGroup;


    QToolButton *fontColorToolButton;
    QToolButton *fillColorToolButton;
    QToolButton *lineColorToolButton;

    QToolBar *textToolBar;
    QToolBar *editToolBar;
    QToolBar *colorToolBar;
    QToolBar *pointerToolbar;
    QToolBar *layerToolbar;

    QWidget *layerWidget;
    QTabWidget *tabWidget;
    QStackedWidget *keyFrameWidget;

    QComboBox *sceneScaleCombo;

    QSlider *interpolationSlider;
    QSlider *keyFrameSlider;

    QLabel *scaleLabel;

    QLayout *keyFrameLayout;


};
#endif // VIEW_H
