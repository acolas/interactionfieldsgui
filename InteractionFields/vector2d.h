#ifndef LIB_VECTOR2D_H
#define LIB_VECTOR2D_H

#include <math.h>

const double PI = 3.1415926535897;

class Vector2D {
 private:
  float x_;
  float y_;

 public:

  Vector2D() {
    x_ = 0.0;
    y_ = 0.0;
  }

  Vector2D(float xx, float yy) {
    x_ = xx;
    y_ = yy;
  }

  Vector2D(Vector2D * v) {
    this->x_ = v->x_;
    this->y_ = v->y_;
  }

  inline float magnitude() const { return sqrtf(x_ * x_ + y_ * y_); }

  inline float sqrMagnitude() const { return (float)(x_ * x_ + y_ * y_); }

  void normalize() {
    float mag = magnitude();
    if(mag > 0){
      x_ /= mag;
      y_ /= mag;
    }
  }
  Vector2D getnormalized() const {
      Vector2D Result(x_, y_);
      Result.normalize();
      return Result;
  }
  float dot(const Vector2D& v) const { return this->x_ * v.x_ + this->y_ * v.y_; }

  void set(float x, float y) {
    this->x_ = x;
    this->y_ = y;
  }

  void setX(float x) {
      this->x_ = x;
  }
  void setY(float y) {
      this->y_ = y;
  }

  inline float x() const{ return x_; }

  inline float y() const{ return y_; }

  inline Vector2D& operator+=(const Vector2D& rhs) {
      x_ += rhs.x(); y_ += rhs.y();
      return *this;
  }

  inline Vector2D& operator-=(const Vector2D& rhs) {
      x_ -= rhs.x(); y_ -= rhs.y();
      return *this;
  }

  inline bool isZero() const { return x_ == 0 && y_ == 0; }
};

Vector2D operator-(Vector2D lhs, Vector2D rhs);
Vector2D operator+(Vector2D lhs, Vector2D rhs);
Vector2D operator*(float lhs, Vector2D rhs);
Vector2D operator*(Vector2D lhs, float rhs);
Vector2D operator/(Vector2D lhs, float rhs);
Vector2D operator-(Vector2D lhs);
double norm(Vector2D x);

inline bool operator==(const Vector2D& p, const Vector2D& q)
{
    return p.x() == q.x() && p.y() == q.y();
}

inline bool operator!=(const Vector2D& p, const Vector2D& q)
{
    return p.x() != q.x() || p.y() != q.y();
}

inline Vector2D rotateCounterClockwise(const Vector2D& v, float radians)
{
    const double Cos = cos(radians), Sin = sin(radians);
    return Vector2D((float)(Cos*v.x() - Sin * v.y()), (float)(Sin*v.x() + Cos * v.y()));
}

inline Vector2D rotateCounterClockwiseAroundPoint(const Vector2D& v, const Vector2D& pivot, float radians)
{
    return pivot + rotateCounterClockwise(v - pivot, radians);
}

inline float angle(const Vector2D& va, const Vector2D& vb)
{
    const double lengths = va.magnitude() * vb.magnitude();
    if (lengths == 0)
        return 0;

    return (float)acos(va.dot(vb) / lengths);
}

inline float cosAngle(const Vector2D& va, const Vector2D& vb)
{
    return cosf(angle(va, vb));
}

inline float sinAngle(const Vector2D& va, const Vector2D& vb)
{
    return sinf(angle(va, vb));
}
inline bool isClockwise(const Vector2D &vector1, const Vector2D &vector2)
{
    float cross = vector1.x() * vector2.y() - vector1.y() * vector2.x();
    return cross > 0;
}

inline bool isCounterClockwise(const Vector2D &vector1, const Vector2D &vector2)
{
    float cross = vector1.x() * vector2.y() - vector1.y() * vector2.x();
    return cross > 0;
}
inline float clockwiseAngle(const Vector2D& va, const Vector2D& vb)
{
    float ang = angle(va, vb);
    if (isCounterClockwise(va, vb))
        return -ang;
    return ang;
}
inline float counterClockwiseAngle(const Vector2D& va, const Vector2D& vb)
{
    float ang = angle(va, vb);
    if (isClockwise(va, vb))
        return -ang;
    return ang;
}

template<typename T>
T clamp(T v, T min, T max) {
    if (v < min) {
        return min;
    }
    if (v > max) {
        return max;
    }
    return v;
}

inline Vector2D clamp(const Vector2D& v, float maxLength)
{
    float mag = v.magnitude();
    if (mag <= maxLength)
        return v;
    return v.getnormalized() * maxLength;
}

inline bool getLineIntersection(const Vector2D& a, const Vector2D& b, const Vector2D& c, const Vector2D& d,	Vector2D& result)
{
    // Line AB represented as a1x + b1y = c1
    double a1 = b.y() - a.y();
    double b1 = a.x() - b.x();
    double c1 = a1 * a.x() + b1 * a.y();

    // Line CD represented as a2x + b2y = c2
    double a2 = d.y() - c.y();
    double b2 = c.x() - d.x();
    double c2 = a2 * c.x() + b2 * c.y();

    double determinant = a1 * b2 - a2 * b1;

    if (fabs(determinant) < 0.00001) // The lines are parallel.
        return false;

    result = Vector2D(
        (float)((b2*c1 - b1 * c2) / determinant),
        (float)((a1*c2 - a2 * c1) / determinant)
    );

    return true;
}

#endif // LIB_VECTOR2D_H
