/********************************************************************************
** Form generated from reading UI file 'view.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEW_H
#define UI_VIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_View
{
public:
    QAction *actionLine;
    QAction *actionCurve;
    QAction *actionAgent;
    QAction *actionObstacle;
    QAction *actionPoles;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QGraphicsView *graphicsView;
    QMenuBar *menubar;
    QMenu *menuTools;
    QMenu *menuObjects;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *View)
    {
        if (View->objectName().isEmpty())
            View->setObjectName(QString::fromUtf8("View"));
        View->resize(1308, 635);
        actionLine = new QAction(View);
        actionLine->setObjectName(QString::fromUtf8("actionLine"));
        actionCurve = new QAction(View);
        actionCurve->setObjectName(QString::fromUtf8("actionCurve"));
        actionAgent = new QAction(View);
        actionAgent->setObjectName(QString::fromUtf8("actionAgent"));
        actionObstacle = new QAction(View);
        actionObstacle->setObjectName(QString::fromUtf8("actionObstacle"));
        actionPoles = new QAction(View);
        actionPoles->setObjectName(QString::fromUtf8("actionPoles"));
        centralwidget = new QWidget(View);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        graphicsView = new QGraphicsView(centralwidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        gridLayout->addWidget(graphicsView, 0, 1, 1, 1);

        View->setCentralWidget(centralwidget);
        menubar = new QMenuBar(View);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1308, 26));
        menuTools = new QMenu(menubar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuObjects = new QMenu(menubar);
        menuObjects->setObjectName(QString::fromUtf8("menuObjects"));
        View->setMenuBar(menubar);
        statusbar = new QStatusBar(View);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        View->setStatusBar(statusbar);

        menubar->addAction(menuTools->menuAction());
        menubar->addAction(menuObjects->menuAction());
        menuTools->addAction(actionLine);
        menuTools->addAction(actionCurve);
        menuObjects->addAction(actionAgent);
        menuObjects->addAction(actionPoles);

        retranslateUi(View);

        QMetaObject::connectSlotsByName(View);
    } // setupUi

    void retranslateUi(QMainWindow *View)
    {
        View->setWindowTitle(QCoreApplication::translate("View", "View", nullptr));
        actionLine->setText(QCoreApplication::translate("View", "Line", nullptr));
        actionCurve->setText(QCoreApplication::translate("View", "Curve", nullptr));
        actionAgent->setText(QCoreApplication::translate("View", "Source", nullptr));
        actionObstacle->setText(QCoreApplication::translate("View", "Obstacle", nullptr));
        actionPoles->setText(QCoreApplication::translate("View", "Poles", nullptr));
        menuTools->setTitle(QCoreApplication::translate("View", "Tools", nullptr));
        menuObjects->setTitle(QCoreApplication::translate("View", "Objects", nullptr));
    } // retranslateUi

};

namespace Ui {
    class View: public Ui_View {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEW_H
