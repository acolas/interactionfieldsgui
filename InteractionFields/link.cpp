#include "link.h"
#include "pole.h"

#include <QPainter>
#include <QPen>
#include <QtMath>

//! [0]
/**
 * @brief constructor of a link
 *
 * @param type type of link: only the rotation link is connected
 * @param startItem the first pole of the link
 * @param endItem the second pole of the link
 * @param parent
 */
Link::Link(LinkType type, Pole *startItem, Pole *endItem, QGraphicsItem *parent)
    : QGraphicsLineItem(parent), myStartItem(startItem), myEndItem(endItem), myType(type)
{
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setPen(QPen(myColor, 0.05, Qt::DashLine, Qt::RoundCap, Qt::RoundJoin));
    this->setZValue(1);
    switch(linkType()){

        case Rotation:{
            typeString="r";
            break;}
        case Translation:{

            typeString ="t";
            break;
        }
        case Scaling:{
            typeString ="s";
            break;
        }
    }

}
//! [0]

//! [1]
/**
 * @brief the bounding rect of the link: is used to select the link
 *
 * @return QRectF
 */
QRectF Link::boundingRect() const
{
    qreal extra = (pen().width() + 2) / 2.0;
    QPointF p1bis = line().p1() + (line().p2()-line().p1())/5.0;
    QPointF p2bis =  line().p2() + (line().p1()-line().p2())/5.0;
    return QRectF(p1bis, QSizeF((p2bis.x() - p1bis.x()),
                                      p2bis.y() - p1bis.y()))
        .normalized()
        .adjusted(-extra, -extra, extra, extra);

    /*
    return QRectF(line().p1(), QSizeF((line().p2().x() - line().p1().x()),
                                      line().p2().y() - line().p1().y()))
        .normalized()
        .adjusted(-extra, -extra, extra, extra);*/
}
//! [1]

//! [2]
/**
 * @brief
 *
 * @return QPainterPath
 */
QPainterPath Link::shape() const
{
    QPainterPath path = QGraphicsLineItem::shape();
    path.addPolygon(arrowHead);
    path.addPolygon(arrowHead2);
    return path;
}
//! [2]

//! [3]
/**
 * @brief
 *
 */
void Link::updatePosition()
{
    QLineF line(mapFromItem(myStartItem, 0, 0), mapFromItem(myEndItem, 0, 0));
    setLine(line);
}
//! [3]

//! [4]
/**
 * @brief add the link item to the renderArea scene
 *
 * @param painter
 * @param
 * @param
 */
void Link::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                  QWidget *)
{
    if (myStartItem->collidesWithItem(myEndItem))
        return;

    QPen myPen = QPen(myColor, 0.05, Qt::DashLine);

    qreal arrowSize =2;
    painter->setPen(myPen);
    painter->setBrush(myColor);
//! [4] //! [5]

    QLineF centerLine(myStartItem->pos(), myEndItem->pos());
    QPolygonF endPolygon = myEndItem->polygon();
    QPointF p1 = endPolygon.first() + myEndItem->pos();
    QPointF intersectPoint;
    for (int i = 1; i < endPolygon.count(); ++i) {
        QPointF p2 = endPolygon.at(i) + myEndItem->pos();
        QLineF polyLine = QLineF(p1, p2);
        QLineF::IntersectionType intersectionType =
            polyLine.intersects(centerLine, &intersectPoint);
        if (intersectionType == QLineF::BoundedIntersection)
            break;
        //p1 = p2;
    }

    setLine(QLineF(intersectPoint, myStartItem->pos()));
//! [5] //! [6]

    double angle = std::atan2(-line().dy(), line().dx());



    QPainterPath path;
    arrowHead.clear();
    arrowHead2.clear();
    switch (myType){
        case Rotation:{
        painter->setBrush(Qt::NoBrush);
        qreal arrowSize= 0.1;
        QPointF arrowP1 = line().p1() + QPointF(sin(angle + M_PI / 6) *arrowSize,
                                        cos(angle + M_PI / 6) * arrowSize);
        QPointF arrowP2 =  line().p1() + QPointF(sin(angle + M_PI - M_PI / 6) * arrowSize,
                                        cos(angle + M_PI - M_PI / 6) *arrowSize);
        angle = std::atan2(-QLineF(line().p1(), arrowP1).dy(), line().dx());
        double angle2 = std::atan2(-QLineF(line().p1(), arrowP2).dy(), line().dx());
        QPointF arrowP3 = arrowP1 + QPointF(sin(angle + M_PI / 6) *arrowSize,
                                           cos(angle + M_PI / 6) * arrowSize);
        QPointF arrowP4 = arrowP2 + QPointF(sin(angle2 +  M_PI - M_PI / 6) *arrowSize,
                                           cos(angle2 + M_PI - M_PI / 6) * arrowSize);
        angle = std::atan2(-QLineF(arrowP1, arrowP3).dy(), QLineF(arrowP1, arrowP3).dx());
        angle2 = std::atan2(-QLineF(arrowP2, arrowP4).dy(), QLineF(arrowP2, arrowP4).dx());
       /* QPointF arrowP5 = arrowP3 + QPointF(sin(angle + M_PI / 6) *arrowSize,
                                           cos(angle + M_PI / 6) * arrowSize);
        QPointF arrowP6 = arrowP4 + QPointF(sin(angle2 + M_PI - M_PI / 6) *arrowSize,
                                           cos(angle2 + M_PI - M_PI / 6) * arrowSize);*/

        /*
        QPointF arrowP1 =line().p1() + QPointF(sin(angle - (M_PI/2+ M_PI/6) ) *arrowSize,
                                        cos(angle - (M_PI/2+ M_PI/6)) * arrowSize);
//        QPointF arrowP2 =  line().p1() + QPointF(sin(angle + (M_PI/2+ M_PI/6)) *arrowSize,
//                                        cos(angle +(M_PI/2+ M_PI/6)) *arrowSize);
        QPointF arrowP3 =  line().p1() + QPointF(sin(angle + (M_PI/2- M_PI/6)) *arrowSize,
                                        cos(angle +(M_PI/2- M_PI/6)) *arrowSize);
//        QPointF arrowP4 =  line().p1() + QPointF(sin(angle - (M_PI/2- M_PI/6)) *arrowSize,
//                                        cos(angle -(M_PI/2- M_PI/6)) *arrowSize);
*/


//        QPointF arrowP1 = QPointF(sin(angle + M_PI / 3) *arrowSize,
//                                        cos(angle + M_PI / 3) * arrowSize);
//        QPointF arrowP2 =  QPointF(sin(angle + M_PI - M_PI / 3) * arrowSize,
//                                        cos(angle + M_PI - M_PI / 3) *arrowSize);
        path.moveTo(line().p1());
        //path.arcMoveTo(QRectF(arrowP1, arrowP2),0);

        path.setFillRule(Qt::OddEvenFill);
        //path.arcTo(QRectF(arrowP1, arrowP2),0,-180);
        //path.addRect(QRectF(arrowP1, arrowP3));
        float radius = 0.5;
        QRectF bounds(line().p1().x()-radius, line().p1().y()-radius,2*radius, 2*radius);
        if(line().p2().y()<line().p1().y()){
            path.arcTo(bounds, angle + M_PI / 6,  -200);
        }
        else{
            path.arcTo(bounds, angle + M_PI / 6,  200);
        }

        //path.lineTo(arrowP1);
        arrowHead = path.toFillPolygon();
        //arrowHead <<line().p1()<<arrowP1<<arrowP3<<arrowP5;


        setColor(Qt::red);
        break;}
    case Translation:{



        float coordTranslateX = line().p2().x() + (line().p1().x()-line().p2().x())/(float)2;
        float coordTranslateY = line().p2().y()  +(line().p1().y() - line().p2().y())/(float)2;
        QPointF arrowP1 = QPointF(coordTranslateX, coordTranslateY) + QPointF(sin(angle + M_PI / 3) * line().length()/(float)3,
                                        cos(angle + M_PI / 3) * line().length()/(float)3);
        QPointF arrowP2 =  QPointF(coordTranslateX, coordTranslateY)  + QPointF(sin(angle + M_PI - M_PI / 3) * line().length()/(float)3,
                                        cos(angle + M_PI - M_PI / 3) *line().length()/(float)3);

        float coordTranslateX1 = line().p2().x() + (line().p1().x()-line().p2().x())/(float)3;
        float coordTranslateY1 = line().p2().y()  +(line().p1().y() - line().p2().y())/(float)3;
        QPointF arrowP3 = QPointF(coordTranslateX1, coordTranslateY1) + QPointF(sin(angle + M_PI / 3) * line().length()/(float)3,
                                        cos(angle + M_PI / 3) * line().length()/(float)3);
        QPointF arrowP4 =  QPointF(coordTranslateX1, coordTranslateY1)  + QPointF(sin(angle + M_PI - M_PI / 3) * line().length()/(float)3,
                                        cos(angle + M_PI - M_PI / 3) *line().length()/(float)3);


        arrowHead << arrowP1 << arrowP2 ;
        arrowHead2 << arrowP3<<arrowP4;


        setColor(Qt::green);
        break;
        }
        case Scaling:{


        QPointF arrowP1 =line().p1() + QPointF(sin(angle + M_PI / 3) *arrowSize,
                                        cos(angle + M_PI / 3) * arrowSize);
        QPointF arrowP2 =  line().p1() + QPointF(sin(angle + M_PI - M_PI / 3) * arrowSize,
                                        cos(angle + M_PI - M_PI / 3) *arrowSize);
        double angle = std::atan2(line().dy(), -line().dx());
        QPointF arrowP3 = line().p2() + QPointF(sin(angle + M_PI / 3) * arrowSize,
                                        cos(angle + M_PI / 3) * arrowSize);
        QPointF arrowP4 = line().p2() + QPointF(sin(angle + M_PI - M_PI / 3) * arrowSize,
                                        cos(angle + M_PI - M_PI / 3) * arrowSize);


         arrowHead <<line().p1()<<arrowP1 << arrowP2;
         arrowHead2 <<line().p2()<<arrowP3<<arrowP4;


        setColor(Qt::blue);


        break;

        }

    }





    painter->drawLine(line());
    //painter->drawPolygon(arrowHead);
    //painter->drawPolygon(arrowHead2);
    if (isSelected()) {
        painter->setPen(QPen(myColor, 0.1, Qt::DashLine));
        QLineF myLine = line();
        myLine.translate(0, 0.4);
        painter->drawLine(myLine);
        myLine.translate(0,-0.8);
        painter->drawLine(myLine);
    }
}

/**
 * @brief
 *
 * @return float
 */
float Link::getAngle() const
{
    return angle;
}

/**
 * @brief
 *
 * @param value
 */
void Link::setAngle(float value)
{
    angle = value;
}

/**
 * @brief
 *
 * @return std::string
 */
std::string Link::getTypeString() const
{
    return typeString;
}
//! [7]
