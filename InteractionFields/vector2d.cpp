#include "vector2d.h"

Vector2D operator-(Vector2D lhs, Vector2D rhs) {
    return Vector2D(lhs.x() - rhs.x(), lhs.y() - rhs.y());
}
Vector2D operator+(Vector2D lhs, Vector2D rhs) {
    return Vector2D(lhs.x() + rhs.x(), lhs.y() + rhs.y());
}
Vector2D operator*(float lhs, Vector2D rhs) {
    return Vector2D(rhs.x() * lhs, rhs.y() * lhs);
}
Vector2D operator*(Vector2D lhs, float rhs) {
    return rhs * lhs;
}
Vector2D operator/(Vector2D lhs, float rhs) {
    return (1.f / rhs) * lhs;
}
double norm(Vector2D x)
{
    double sum2 = x.x() * x.x() + x.y() * x.y();
    return sqrt(sum2);
}

Vector2D operator-(Vector2D lhs)
{
    return Vector2D(-lhs.x(), -lhs.y());
}

float dot(Vector2D lhs, Vector2D rhs)
{
    return lhs.x() * rhs.x() + lhs.y() * rhs.y();
}
