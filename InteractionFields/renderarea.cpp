#include "renderarea.h"
#include "vectorcurve.h"
#include "node.h"
#include <QMouseEvent>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QPointF>
#include <QWidget>
#include <QCursor>
#include <QPainterPath>
#include <vector>
#include <array>
#include <fstream>
#include <QGenericMatrix>
#include <vectorline.h>
#include <algorithm>					
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <QStyleOptionGraphicsItem>								   



using namespace std;


/**
 * @brief constructor of RenderArea
 * Canvas where Interactions Fields are sketched, central scene widget.
 *
 * @param itemMenu menu that is showed when right cliking on some items
 * @param parent QGraphicsView parent
 */
RenderArea::RenderArea(QMenu *itemMenu, QWidget *parent)

    : QGraphicsView(parent)
{

    //setAttribute(Qt::WA_StaticContents);


    QGraphicsScene *scene = new QGraphicsScene(this);

    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(0, 0, 100, 100);
    setScene(scene);


    fitInView(scene->sceneRect(), Qt::KeepAspectRatio);

    //setFixedSize(500,500);
    myItemMenu = itemMenu;
    myMode = MoveItem;
    myItemType = Pole::Agent;
    nextAgentId=0;
    nextObstacleId=0;
    line = nullptr;
    myPenWidth = 0.1;
    brushWidth =1;
    brushIntensity = 1;
	eraserCursor =QCursor(QPixmap(".\\images\\eraser.png"), -2, -2);																
    myPenColor.setHsv(220,250,250);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scene->setMinimumRenderSize(0.0000001);
    //scale(qreal(0.8), qreal(0.8));
    //setMinimumSize(400, 400);
    setWindowTitle(tr("Interaction Fields GUI"));
    scale(1,-1); // change the axes so that the "y" is like in Umans



}
/**
 * @brief
 *
 * @param fileName
 * @return bool
 */
bool RenderArea::openImage(const QString &fileName) // not used for now
//! [1] //! [2]
{
    QImage loadedImage;
    if (!loadedImage.load(fileName))
        return false;

    //QSize newSize = loadedImage.size().expandedTo(size());
    //resizeImage(&loadedImage, newSize);
    image = loadedImage;
    modified = false;
    update();
    return true;
}

/**
 * @brief
 *
 * @param fileName
 * @param fileFormat
 * @return bool
 */
bool RenderArea::saveImage(const QString &fileName, const char *fileFormat)
{
    QImage visibleImage = image;
   //resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
        modified = false;
        return true;
    }
    return false;

}
//! [2]

/**
 * @brief load a field
 *
 * @param filename: the name of the file to load
 */
void RenderArea::loadField(const QString &filename)//load a field in txt format
{
    const QString path = "../examples/interactionFields/matrix/";


        if(layers[layerID]->getIsParametric() ){

            float param = getParameter();
            for(auto &layer : layers[layerID]->getInteractionFields())// if parametric, add a new key field
            {
                float key = layer.first;
                layers[layerID]->setParameter(key);
                layers[layerID]->getCurrentLayer()->generateMatrix(brushIntensity);

                    generateMatrixFromFile(filename);
            }
            layers[layerID]->setParameter(param);
            string a;
        }
        else{
            generateMatrixFromFile(filename);
        }
        //addLayerDisplayed();
        drawMatrix();

}
 /**
  * @brief  generate matrix of the current layer viewer
  *build the matrix of the field and create each vectors accoding to the control curves
  */
 void RenderArea::generate()
{
     for(auto &layer: getLayers()){


    if(layer->getCurrentLayer()->getControlVectors().empty()){
        layer->getCurrentLayer()->removeZeroArea();
    }

    layer->getCurrentLayer()->generateMatrix(brushIntensity);
    drawMatrix();
     }


    //apply(); apply to the simulation scene
}






/**
 * @brief
 *
 * @param newColor
 */
void RenderArea::setPenColor(const QColor &newColor)
//! [5] //! [6]
{
    myPenColor = newColor;
}
//! [6]

//! [7]
/**
 * @brief
 *
 * @param newWidth
 */
void RenderArea::setPenWidth(float newWidth)
//! [7] //! [8]
{
    myPenWidth = newWidth;
}
//! [8]


/**
 * @brief
 *
 * @param layer
 */
void RenderArea::addLayer(LayerGroup *layer)
{
 scene()->addItem(layer);
 scene()->addItem(layer->getCurrentLayer());
 layers.push_back(layer);
 gridShow();
}

//! [9]
/**
 * @brief clear the canvas
 *
 */
void RenderArea::clearImage()// clear the canvas
//! [9] //! [10]
{
    myPenWidth = std::min(sceneRect().width(), sceneRect().height())/10.0f * 0.05;
/*
    layers[layerID]->getCurrentLayer()->clearLayer();

    layers[layerID]->clearGroup();
    scene()->removeItem(layers[layerID]);
    scene()->removeItem(layers[layerID]->getCurrentLayer());
    if(!scene()->children().isEmpty())
        scene()->clear();
    scene()->addItem(layers[layerID]);
    scene()->addItem(layers[layerID]->getCurrentLayer());

    modified = true;*/
    layers[layerID]->clear();


    generate();
    update();
}



//! [6]
/**
 * @brief
 *  start scribble lines or curves
 *  insert an item
 *  use eraser or brush
 * @param event
 */
void RenderArea::mousePressEvent(QMouseEvent *event)
{

    myPenWidth=layers[layerID]->getCurrentLayer()->getMyPenWidth();
    if (event->button() != Qt::LeftButton)// we only trat the left click
        return;
    //only if it is inside the canvas area/scene
    if(myMode!= MoveItem &&(!sceneRect().contains(mapToScene(event->pos()))|| !layers[layerID]->getCurrentLayer()->getBorder().contains((mapToScene(event->pos()))))){

        return;
    }
    switch (myMode) {
        case InsertItem: //insert a pole
            insertPole(mapToScene(event->pos()), myItemType, false);

            break;
//! [6] //! [7]
        case InsertLine: // add a control line
            line = new QGraphicsLineItem(QLineF(mapToScene(event->pos()),
                                        mapToScene(event->pos())));
            line->setPen(QPen(QColor(150,150,150), myPenWidth));//gray
            break;
        case InsertCurve: //add a control cuve
            lastPoint = mapToScene(event->pos()); //current position of the mouse pressed
            startPoint = mapToScene(event->pos()); //first position of the mouse pressed
            path = QPainterPath(startPoint); //create a QPainter path to create the curve
            scribbling=true;
            scribbleLines = new QGraphicsItemGroup();
            scene()->addItem(scribbleLines); //create the scribble line

            break;

        case InsertLink:
            line = new QGraphicsLineItem(QLineF(mapToScene(event->pos()),
                                        mapToScene(event->pos())));
            line->setPen(QPen(QColor(150,150,150), 1.5*myPenWidth));//gray
            break;
        case MoveItem://move around the nodes or the poles
            QGraphicsView::mousePressEvent(event);
            break;
        case EraseItem: //create a zero area
            scribbling = true;
            if(layers[layerID]->getCurrentLayer()->getMatrix() != nullptr){
                LayerViewer *layer = layers[layerID]->getCurrentLayer();
                array<int,2> indexes = layer->getCase(mapToScene(event->pos())); //get the indexes of the position of the mouse in the matrix
                for (int i = -brushWidth/2+1-brushWidth%2; i<brushWidth/2 + brushWidth%2; i++){ // the wide of the eraser depend on the width brush (this still need work)
                    for(int j = -brushWidth/2+1-brushWidth%2;j<brushWidth/2 + brushWidth%2; j++){
                        bool equal = false;
                        for(auto &ind : layer->getZerosArea()){
                            if(std::equal(std::begin(ind), std::end(ind), std::begin(array<int,2> {indexes[0]+i, indexes[1]+j}))){ //check if the index is already in the zero area
                                equal = true;
                                break;
                             }
                        }
                        if(!equal){
                            layer->addZerosArea(array<int,2> {indexes[0]+i, indexes[1]+j}); // if not add it
                            drawMatrix(); //redraw the matrix without the zero area
                        }

                    }
               }
           }
    case ColorBrush: //change the magnitude of the vector colored
        scribbling = true;
        if(layers[layerID]->getCurrentLayer()->getMatrix() != nullptr){
            LayerViewer *layer = layers[layerID]->getCurrentLayer();
            array<int,2> indexes = layer->getCase(mapToScene(event->pos()));//find the index of the mouse postion in the matrix
            for (int i = -brushWidth/2+1-brushWidth%2; i<brushWidth/2 + brushWidth%2; i++){
                for(int j = -brushWidth/2+1-brushWidth%2;j<brushWidth/2 + brushWidth%2; j++){
                    Vector2D oldVect = layer->getMatrix()->at(indexes[0]+i, indexes[1]+j);
                    layer->getMatrix()->setAt(indexes[0]+i, indexes[1]+j, oldVect.getnormalized()*brushIntensity);//change the magnitude
                    drawMatrix();
                }
           }

       }


        break;

//! [7] //! [8]

//! [8] //! [9]
    default:
        ;
    }

    update();
    scene()->update();
}
//! [9]

//! [10]
/**
 * @brief
 *  update the scribble line, the preview lines, moving item.
 *  update the vectors magntiude under the brush tool
 *  add the vectors to the zero area
 *  redraw the matrix
 * @param event
 */
void RenderArea::mouseMoveEvent(QMouseEvent *event)
{
    //take into account only of the mouse is pressed and inseid the scene
    if(myMode!= MoveItem &&(!sceneRect().contains(mapToScene(event->pos()))|| !layers[layerID]->getCurrentLayer()->getBorder().contains((mapToScene(event->pos()))))){
        //setCursor(Qt::ArrowCursor);
        return;
    }

    if (myMode == InsertLine ) {
        //setCursor(Qt::ArrowCursor);
        if(line != nullptr){
            scene()->addItem(line);
            QLineF newLine(line->line().p1(),  mapToScene(event->pos()));//draw preview line
            line->setLine(newLine);
        }

    } else if (myMode == MoveItem) {
        //setCursor(Qt::ArrowCursor);
        if(event->buttons() == Qt::LeftButton){
            QList<QGraphicsItem*> selectedItems = scene()->selectedItems();
            for (QGraphicsItem *item : qAsConst(selectedItems)) {
                 if (item->type() == Pole::Type){
                     for (auto *link : qgraphicsitem_cast<Pole *>(item)->getLinks())
                         if(!layers[layerID]->getIsParametric()) //if links, update the position
                            layers[layerID]->getCurrentLayer()->updateLink(link);
                     scene()->update();
                }

            }
        }
        //generate();//to update the matrix each time we move an item
        QGraphicsView::mouseMoveEvent(event);


    } else if (myMode == InsertCurve){
        //setCursor(Qt::ArrowCursor);
        if(scribbling){

              drawLineTo(mapToScene(event->pos())); //add scribble line
        }
        else{
            return;
        }
    }else if (myMode == InsertLink && line != nullptr){
        //setCursor(Qt::ArrowCursor);
        scene()->addItem(line);
        QLineF newLine(line->line().p1(),  mapToScene(event->pos()));
        line->setLine(newLine);

    }else if (myMode == EraseItem){//the same than if mouse pressed
       if(layers[layerID]->getCurrentLayer()->getMatrix() != nullptr && scribbling){
           LayerViewer *layer = layers[layerID]->getCurrentLayer();
           array<int,2> indexes = layer->getCase(mapToScene(event->pos()));
           for (int i = -brushWidth/2; i<=brushWidth/2; i++){
               for(int j = -brushWidth/2;j<=brushWidth/2; j++){
                   bool equal = false;
                   for(auto &ind : layer->getZerosArea()){
                       if(std::equal(std::begin(ind), std::end(ind), std::begin(array<int,2> {indexes[0]+i, indexes[1]+j}))){
                           equal = true;
                           break;
                        }
                   }
                   if(!equal)
                        layer->addZerosArea(array<int,2> {indexes[0]+i, indexes[1]+j});
                        drawMatrix();
               }
          }

      }

    }else if (myMode == ColorBrush){
        if(layers[layerID]->getCurrentLayer()->getMatrix() != nullptr && scribbling){
            LayerViewer *layer = layers[layerID]->getCurrentLayer();
            array<int,2> indexes = layer->getCase(mapToScene(event->pos()));
            for (int i = -brushWidth/2; i<=brushWidth/2; i++){
                for(int j = -brushWidth/2;j<=brushWidth/2; j++){
                         Vector2D oldVect = layer->getMatrix()->at(indexes[0]+i, indexes[1]+j);
                         layer->getMatrix()->setAt(indexes[0]+i, indexes[1]+j, oldVect.getnormalized()*brushIntensity);
                }
            }
          drawMatrix(); //redraw the matrix with the new magnitudes vector values
       }

   }
    QGraphicsView::mouseMoveEvent(event);

    update();
    scene()->update();
}
//! [10]

//! [11]
/**
 * @brief  add control curves, lines or links and move items
 *
 * @param event
 */
void RenderArea::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton){
        return;
    }
    if(!sceneRect().contains(mapToScene(event->pos()))|| !layers[layerID]->getCurrentLayer()->getBorder().contains((mapToScene(event->pos())))){
        if(line != nullptr){
            scene()->removeItem(line);
            delete line;
        }
        line = nullptr;
        if(scribbleLines != nullptr && scribbling){
            scene()->removeItem(scribbleLines);
            scene()->destroyItemGroup(scribbleLines);//destroy sribbles
        }
        scribbling=false;
        return;
    }

    if (line != nullptr && myMode == InsertLink) { //add the link to the scene
        QList<QGraphicsItem *> startItems = scene()->items(line->line().p1());
        if (startItems.count() && startItems.first() == line)
            startItems.removeFirst();
        QList<QGraphicsItem *> endItems = scene()->items(line->line().p2());
        if (endItems.count() && endItems.first() == line)
            endItems.removeFirst();
        scene()->removeItem(line);
        delete line;


//! [11] //! [12]

        if (startItems.count() > 0 && endItems.count() > 0 &&
            startItems.first()->type() == Pole::Type &&
            endItems.first()->type() == Pole::Type &&
            startItems.first() != endItems.first()) {
            Pole *startItem = qgraphicsitem_cast<Pole *>(startItems.first());
            Pole *endItem = qgraphicsitem_cast<Pole *>(endItems.first());

            if(startItem->type() == Pole::Agent){
                endItem=qgraphicsitem_cast<Pole *>(startItems.first());
                startItem = qgraphicsitem_cast<Pole *>(endItems.first());
            }

            Link *link = new Link( myLinkType, startItem, endItem);
            const Vector2D towardEndItem= Vector2D((endItem->pos()-startItem->pos()).x(),(endItem->pos()-startItem->pos()).y());
            if(myLinkType == Link::Rotation)
                link->setAngle(clockwiseAngle(Vector2D(0,-1),towardEndItem ));

            startItem->addLink(link);
            endItem->addLink(link);
            layers[layerID]->addLink(link);
            //link->setZValue(-1000.0);
            scene()->addItem(link);
            link->updatePosition();


        }
        //generate(); to update the matrix each we add an item




    }

    else if (myMode == InsertCurve){
        if(scribbling==true){
            endPoint = mapToScene(event->pos());//change endPoint
            drawCurveTo(path);//add control curve
            scene()->removeItem(scribbleLines);
            scene()->destroyItemGroup(scribbleLines);

        }
        //generate(); to update the matrix each time we add an item



    }

    else if(line != nullptr && myMode == InsertLine) {
        drawCurveTo(1,line->shape());//add control line
        scene()->removeItem(line);
        delete line;
        //generate();to update the matrix each time we add an item

    }else if(myMode == MoveItem)
    {
        QList<QGraphicsItem*> selectedItems = scene()->selectedItems();
        for (QGraphicsItem *item : qAsConst(selectedItems)) {
             if (item->type() == Pole::Type){
                 //renderArea->getPoles().remove((qgraphicsitem_cast<Pole *>(item)->scenePos()));

                 for (auto *link : qgraphicsitem_cast<Pole *>(item)->getLinks())
                     if(!layers[layerID]->getIsParametric())
                        layers[layerID]->getCurrentLayer()->updateLink(link);
                 scene()->update();
            }


        }
      drawMatrix();
    }



//! [12] //! [13]
    line = nullptr;
    scribbling=false;
    QGraphicsView::mouseReleaseEvent(event);
    layers[layerID]->getCurrentLayer()->setIsModified(true);
    update();


}
//! [13]







/**
 * @brief add a new pole to the scene
 *
 * @param pos position of the new pole
 * @param type of the new pole: obstacle or agent
 * @param source: tell if the pole is source or not
 */
void RenderArea::insertPole(QPointF pos, Pole::Pole_Type type, bool source){

    Pole *item = new Pole(type, myItemMenu, source);
    if(item->PoleType()== Pole::Agent){
        item->setId(nextAgentId);
        nextAgentId ++;


    }
    else{
        item->setId(nextObstacleId);
        nextObstacleId ++;

    }
    QColor poleColor = QColor(30, 220, 255);
    QColor sourceColor = QColor(220, 14, 21);

    if(source){
        item->setBrush(sourceColor);
        item->setFlag(QGraphicsItem::ItemIsSelectable , false);
        item->setFlag(QGraphicsItem::ItemIsMovable, false);
    }
    else{
        item->setBrush(poleColor);
    }
    item->setRotation(item->rotation()+180);
    item->setPen(QPen(Qt::black, 0.05));
    item->setScale(0.24);

    item->setPos(pos);
    layers[layerID]->addPole(item);
    scene()->addItem(item);
    emit itemInserted(item);
}



/**
 * @brief
 *  if key "escape": deselect all
 *  if key "arrow up": increase the number of nodes of the selected control curves or increase the height of the selected obstacles
 *  if key "arrow down": decrease the number of nodes of the selected control curves or decrease the height of the selected obstacles
 *  if key "arrow right": increase the width of the selected obstacles
 *  if key "arrow left": deccrease the width of the selected obstacles
 *
 * @param event
 */
void RenderArea::keyPressEvent(QKeyEvent *event){

    if(event->key()==Qt::Key_Escape){
        for (auto &vectors : scene()->selectedItems() ){
            vectors->setSelected(false);
        }
    }
    else if(event->key()==Qt::Key_Up){


        for(auto &item : scene()->selectedItems()){
            if (item->type() == Pole::Type) {
                qgraphicsitem_cast<Pole *>(item)->changeSize(0.1, 0);
                if( qgraphicsitem_cast<Pole *>(item)->getIsSource()){
                    updateObstacleSize(item->pos(),  qgraphicsitem_cast<Pole *>(item)->getHeight(),  qgraphicsitem_cast<Pole *>(item)->getWidth(), 0.1,0);
                }

            }
            if(item->type() == VectorCurve::Type){
                VectorCurve *curve = qgraphicsitem_cast<VectorCurve *>(item);
                if(curve->getControlPts().length()<40){

                    curve->setCurvePts(curve->getPath(), curve->getControlPts().length() );
                        for(auto &node : curve->getNodes() ){
                            scene()->removeItem(node);
                            node->update();
                         }
                        curve->createNodes();

                        for(auto &node : curve->getNodes() ){
                            scene()->addItem(node);
                            node->update();
                         }
                        curve->update();
                    }
                }

            }
    }
    else if(event->key()==Qt::Key_Down){

        for(auto &item : scene()->selectedItems()){
            if (item->type() == Pole::Type) {
                qgraphicsitem_cast<Pole *>(item)->changeSize(-0.1, 0);
                if( qgraphicsitem_cast<Pole *>(item)->getIsSource()){
                    updateObstacleSize(item->pos(),  qgraphicsitem_cast<Pole *>(item)->getHeight(),  qgraphicsitem_cast<Pole *>(item)->getWidth(),-0.1,0 );
                }
            }
            if(item->type() == VectorCurve::Type){
                VectorCurve *curve = qgraphicsitem_cast<VectorCurve *>(item);
                int length = curve->getControlPts().length();

                if(curve->getControlPts().length()>2){
                    curve->setCurvePts(curve->getPath(), length-2);
                    //curve->adjustNodes()


                    for(auto &node : curve->getNodes() ){
                        scene()->removeItem(node);
                        node->update();
                     }
                    curve->createNodes();
                    for(auto &node : curve->getNodes() ){
                        scene()->addItem(node);
                        node->update();
                     }
                    curve->update();
                }
            }

        }
    }
    else if(event->key()==Qt::Key_Left){
        for(auto &item : scene()->selectedItems()){
            if (item->type() == Pole::Type) {
                qgraphicsitem_cast<Pole *>(item)->changeSize(0, -0.1);
                if( qgraphicsitem_cast<Pole *>(item)->getIsSource()){
                    updateObstacleSize(item->pos(),  qgraphicsitem_cast<Pole *>(item)->getHeight(),  qgraphicsitem_cast<Pole *>(item)->getWidth(), 0,-0.1);
                }

            }
        }
    }
    else if(event->key()==Qt::Key_Right){
        for(auto &item : scene()->selectedItems()){
            if (item->type() == Pole::Type) {
                qgraphicsitem_cast<Pole *>(item)->changeSize(0, 0.1);
                if( qgraphicsitem_cast<Pole *>(item)->getIsSource()){
                    updateObstacleSize(item->pos(),  qgraphicsitem_cast<Pole *>(item)->getHeight(),  qgraphicsitem_cast<Pole *>(item)->getWidth(), 0,0.1);
                }

            }
        }
    }



    update();
    scene()->update();
}




//! [7]
/**
 * @brief zoom in or zoom out
 *
 * @param scaleFactor
 */
void RenderArea::scaleView(qreal scaleFactor)
{
    scale(scaleFactor, scaleFactor);
}




void RenderArea::wheelEvent(QWheelEvent *event)
{
    scaleView(pow(2., event->angleDelta().y() / 240.0));
    //eraserCursor = cursorBrushUpdate();
}



/**
 * @brief show the grid of the matrix by creating lines
 *
 */
void RenderArea::gridShow()
{
    QRectF border = layers[layerID]->getCurrentLayer()->getBorder();
    float height = border.height();
    float width = border.width();
    QPointF start = border.bottomLeft();

    for(float x = start.x() ; x< width+start.x(); x+=(float)width/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols()){
        QGraphicsLineItem *line = new QGraphicsLineItem(x,start.y(),x,start.y()-height);

        //scene()->addLine(x,start.y(),x,start.y()-height, QPen(Qt::gray,0.01));
        line->setPen(QPen(Qt::gray,0.01));
        line->setZValue(-2);
        scene()->addItem(line);
        layers[layerID]->addToGroup(line);
    }
    for (float y = start.y(); y>start.y()-height; y-=(float)height/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows()){
        //scene()->addLine(start.x(),y,start.x()+width,y, QPen(Qt::gray,0.01));
        QGraphicsLineItem *line = new QGraphicsLineItem(start.x(),y,start.x()+width,y);
        line->setPen(QPen(Qt::gray,0.01));
        line->setZValue(-2);
        scene()->addItem(line);
        layers[layerID]->addToGroup(line);
    }

}

/**
 * @brief show the grid of the matrix by creating rectangles
 *
 */
void RenderArea::gridCreate(){
    QRectF border = layers[layerID]->boundingRect();
    float height = border.height();
    float width = border.width();
    QPointF start = border.bottomLeft();

    scene()->addLine(QLineF(start, start));
    for (int i = 0; i*height/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows()< height; i++){

        for(int j = 0; j*width/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols()< width; j++){
            double y = start.y() - (float)i*height/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols();
            double x = start.x()+ (float) j *width/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows();
            double yf = y - height/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols();
            double xf = x + width/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows();

            //auto rect = new QRectF(QPointF(x,y), QPointF(xf,yf));
            scene()->addRect(QRectF(QPointF(x,y), QPointF(xf,yf)), QPen(Qt::gray,0.01));

//            grid[i][j]= QRectF(QPointF(x,y), QPointF(xf,yf));
        }

    }
}
/**
 * @brief draw a matrix
 *
 * @param matrix: matrix to be drawn
 * @param color: color in which to draw the vectors
 */
void RenderArea::drawMatrix(VectorMatrix* matrix, QColor color){
    QRectF border = layers[layerID]->getCurrentLayer()->getBorder();
    gridShow();
    double height = border.height();
    double width = border.width();
    QPointF start = border.bottomLeft();
    for (int i = 0; i*height/(float)matrix->getRows()< height; i++){

        for(int j = 0; j*width/(float)matrix->getCols()< width; j++){
            float y = start.y() - (float)i*height/(float)matrix->getRows();
            float x = start.x()+ (float) j *width/(float)matrix->getCols();
            float yf = y - height/(float)matrix->getRows();
            float xf = x + width/(float)matrix->getCols();


            QRectF rect = QRectF(QPointF(x,y), QPointF(xf,yf));
            Vector2D vector = matrix->at(i,j);
            //color.setHsv(220,std::min(vector.sqrMagnitude(), 1.8f)*100+70,250);
            qreal scaling = std::min(std::abs(rect.height()/2.2), std::abs(rect.width()/2.2));
            vector = matrix->at(i,j)*scaling/1.8f;

            QGraphicsLineItem *line = new QGraphicsLineItem(QLineF(rect.center(),rect.center() + QPointF(vector.x(), vector.y())));

            if(line->line().length()>0.0001){
                line->setPen(QPen(color,myPenWidth/3.0));
                //layers[layerID]->addToGroup(line);
                scene()->addItem(line);

                double angle = std::atan2(-line->line().dy(), line->line().dx());

                QPointF destArrowP1 = line->line().p2() + QPointF(sin(angle - M_PI / 3) * myPenWidth*2,
                                                          cos(angle - M_PI / 3) * myPenWidth*2);
                QPointF destArrowP2 = line->line().p2() + QPointF(sin(angle - M_PI + M_PI / 3) * myPenWidth*2,
                                                          cos(angle - M_PI + M_PI / 3) * myPenWidth*2);
                QGraphicsPolygonItem *arrow = new QGraphicsPolygonItem(QPolygonF() << line->line().p2() << destArrowP1 << destArrowP2);
                arrow->setPen(QPen(color,myPenWidth/3.));
                QBrush brush = QBrush(color);
                brush.setStyle(Qt::SolidPattern);


                arrow->setBrush(brush);
                //layers[layerID]->addToGroup(arrow);
                scene()->addItem(arrow);

            }
        }
    }
}

/**
 * @brief draw the current matrix
 *  current matrix is the one of the current layer view of the current layergroup
 */
void RenderArea::drawMatrix()
{

    myPenWidth = layers[layerID]->getCurrentLayer()->getMyPenWidth();



    //checkCombination();
    for(auto& item : layers[layerID]->childItems()){
        if(item->type() != VectorLine::Type && item->type() != VectorCurve::Type && item->type() != Pole::Type && item->type() != Link::Type && item->type()!=Node::Type){
            scene()->removeItem(item);
            layers[layerID]->getCurrentLayer()->removeFromGroup(item);
            delete item;
        }

    }

    for(auto &field : layers[layerID]->getInteractionFields()){
        for(auto& item : field.second->childItems()){
            if(item->type() != VectorLine::Type && item->type() != VectorCurve::Type && item->type() != Pole::Type && item->type() != Link::Type && item->type()!=Node::Type){
                field.second->removeFromGroup(item);
                scene()->removeItem(item);
                layers[layerID]->getCurrentLayer()->removeFromGroup(item);
                delete item;
            }
        }
    }


    if (layers[layerID]->getIsGridVisible())
        gridShow();


    QRectF border = layers[layerID]->getCurrentLayer()->getBorder();

    float height = border.height();
    float width = border.width();
    QPointF start = border.bottomLeft();
    float offseti =height/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows();
    float offsetj=width/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols();
    for (int i = 0; i< (float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows(); i++){

        for(int j = 0; j< (float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols(); j++){
            float y = start.y() - (float)i*offseti;
            float x = start.x()+ (float) j *offsetj;
            float yf = y - height/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getRows();
            float xf = x + width/(float)layers[layerID]->getCurrentLayer()->getMatrix()->getCols();
            /*if (std::find(layers[layerID]->getCurrentLayer()->getZerosArea().begin(), layers[layerID]->getCurrentLayer()->getZerosArea().end(),indexCurrent)!=layers[layerID]->getCurrentLayer()->getZerosArea().end()){
                printf("in zero area");
            }*/
             //tried to use a function to check if zerosArea contains the current index but didn't work so I had to it by "hand"
            bool notZero = true;
            for (auto &indexe : layers[layerID]->getCurrentLayer()->getZerosArea()){
                if(indexe == array<int,2>{i,j}){
                    notZero=false;
                }
            }
           if (notZero){

                QRectF rect = QRectF(QPointF(x,y), QPointF(xf,yf));


                Vector2D vector = layers[layerID]->getCurrentLayer()->getMatrix()->at(i,j);

                //color = Qt::gray;

                qreal rv =(vector.magnitude()-0.1)*100.0 / (100-10);
                //QColor c = QColor(from.red()*rv + to.red()*(1.0-rv),from.green()*rv + to.green()*(1.0-rv), from.blue()*rv + to.blue()*(1.0-rv) );
                QColor color = QColor::fromHsl(205 - 205  * rv, 200, 135);
                //color.setHsv(220,std::min(vector.sqrMagnitude(), 1.8f)*100+70,250);
                qreal scaling = std::min(std::abs(rect.height()/2.2), std::abs(rect.width()/2.2));
                vector = layers[layerID]->getCurrentLayer()->getMatrix()->at(i,j).getnormalized()*scaling;

                QGraphicsLineItem *line = new QGraphicsLineItem(QLineF(rect.center(),rect.center() + QPointF(vector.x(), vector.y())));

                if(line->line().length()>0.0001){
                    line->setPen(QPen(color,myPenWidth/2.0));
                    layers[layerID]->addToGroup(line);
                    scene()->addItem(line);

                    double angle = std::atan2(-line->line().dy(), line->line().dx());

                    QPointF destArrowP1 = line->line().p2() + QPointF(sin(angle - M_PI / 3) * myPenWidth*2,
                                                              cos(angle - M_PI / 3) * myPenWidth*2);
                    QPointF destArrowP2 = line->line().p2() + QPointF(sin(angle - M_PI + M_PI / 3) * myPenWidth*2,
                                                              cos(angle - M_PI + M_PI / 3) * myPenWidth*2);
                    QGraphicsPolygonItem *arrow = new QGraphicsPolygonItem(QPolygonF() << line->line().p2() << destArrowP1 << destArrowP2);
                    arrow->setPen(QPen(color,myPenWidth/2.));
                    layers[layerID]->addToGroup(arrow);
                    scene()->addItem(arrow);

                }
           }

        }
    }

}

/**
 * @brief check if there are overlapping fields that need to be combined
 *
 */
void RenderArea::checkCombination(){//not done yet
    if(layers.size()>1){
        for(int i=0; i<layers.size(); i++){
            for(int j=i+1 ; j<layers.size(); j++){
                if(layers[i]->getCurrentLayer()->getBorder().intersects(layers[i]->getCurrentLayer()->getBorder())){
                    LayerGroup *layerA = layers[j];
                    LayerGroup *layerB = layers[i];
                    if(layers[i]->getCurrentLayer()->getHeight()+layers[i]->getCurrentLayer()->getWidth()>=layers[j]->getCurrentLayer()->getHeight()+layers[j]->getCurrentLayer()->getWidth()){

                        layerA = layers[i];
                        layerB = layers[j];

                    }

                    layerA->combine(layerB);

                }
            }
        }
    }

}

/**
 * @brief read matrix from txt file
 *
 * @param fileName name of the txt file field
 */
void RenderArea::generateMatrixFromFileOld(const QString &fileName)
{


    //this->clear();
    string line;
    //ifstream myfile(fileName.toUtf8().constData());
    ifstream myfile(fileName.toUtf8().constData());



    if (myfile.is_open())
    {

        cout << "File open";
        if (getline(myfile, line)) {
            size_t pos = 0;
            string token;
            pos = line.find(";");
            string test = line.substr(0, pos);
            string test2 = line.substr(pos + 1, line.length());

            int row = stoi(line.substr(0, pos), nullptr, 10);
            line.erase(0, line.find(";") + 1);
            pos = line.find(";");
            int col = stoi(line.substr(0, pos), nullptr, 10);
            line.erase(0, line.find(";") + 1);
            pos = line.find(";");
            float subheight = stof( line.substr(0,pos));
            line.erase(0, line.find(";") + 1);
            pos = line.find(";");
            float subwidth =  stof(line.substr(0, pos));
            layers[layerID]->getCurrentLayer()->setWidth(subwidth*(float)col);
            layers[layerID]->getCurrentLayer()->setHeight(subheight*(float)row);
            layers[layerID]->getCurrentLayer()->setBorder(QRectF(sceneRect().center()-QPointF(layers[layerID]->getCurrentLayer()->getWidth()/2., layers[layerID]->getCurrentLayer()->getHeight()/2.),QSize(layers[layerID]->getCurrentLayer()->getWidth(), layers[layerID]->getCurrentLayer()->getHeight())));
            VectorMatrix *matrix = new VectorMatrix(row,col);
            for (int i = 0; i < row; i++) {

                for (int j = 0; j < col; j++) {
                    if (getline(myfile, line)) {
                            pos = line.find(";");
                            float x = stof(line.substr(0, pos));
                            float y = stof(line.substr(pos + 1, line.length()));
                            Vector2D vect = Vector2D(x, y);
                            matrix->setAt(i, j, vect);
                    }
                }
            }
            layers[layerID]->getCurrentLayer()->setMatrix(matrix);
            myfile.close();
        }

        else {
            cout << "Error getline";
        }


        }



    else {
    cout << "Unable to open file";

    }
    drawMatrix();




}

/**
 * @brief read matrix from txt file
 *
 * @param fileName fileName name of the txt file field
 * @return VectorMatrix return the resulting matrix
 */
VectorMatrix* RenderArea::getMatrixFromFileOld(const QString &fileName)
{


    //this->clear();
    string line;
    ifstream myfile(fileName.toUtf8().constData());



    if (myfile.is_open())
    {

        cout << "FIle open";
        if (getline(myfile, line)) {
            size_t pos = 0;
            string token;
            pos = line.find(";");
            string test = line.substr(0, pos);
            string test2 = line.substr(pos + 1, line.length());

            int row = stoi(line.substr(0, pos), nullptr, 10);
            line.erase(0, line.find(";") + 1);
            pos = line.find(";");
            int col = stoi(line.substr(0, pos), nullptr, 10);
            line.erase(0, line.find(";") + 1);
            pos = line.find(";");
            float subheight = stof( line.substr(0,pos));
            line.erase(0, line.find(";") + 1);
            pos = line.find(";");
            float subwidth =  stof(line.substr(0, pos));
            LayerViewer *resultLayer = new LayerViewer(subwidth*(float)col, subheight*(float)row);
            resultLayer->setBorder(QRectF(sceneRect().center()-QPointF(layers[layerID]->getCurrentLayer()->getWidth()/2., layers[layerID]->getCurrentLayer()->getHeight()/2.),QSize(layers[layerID]->getCurrentLayer()->getWidth(), layers[layerID]->getCurrentLayer()->getHeight())));
            VectorMatrix *matrix = new VectorMatrix(row,col);

            for (int i = 0; i < row; i++) {

                for (int j = 0; j < col; j++) {
                    if (getline(myfile, line)) {


                            pos = line.find(";");
                            float x = stof(line.substr(0, pos));
                            float y = stof(line.substr(pos + 1, line.length()));
                            Vector2D vect = Vector2D(x, y);
                            matrix->setAt(i, j, vect);




                    }
                }
            }
            resultLayer->setMatrix(matrix);
            return matrix;
            myfile.close();
        }

        else {
            cout << "Error getline";
        }


        }



    else {
    cout << "Unable to open file";

    }

}



/**
 * @brief read txt field files considering that only the control curves were saved (the continuous version)
 * @param fileName fileName name of the txt file field
 */
void RenderArea::generateMatrixFromFile(const QString &fileName)
{

    //this->clear();
    string line;
    ifstream myfile(fileName.toUtf8().constData());



    if (myfile.is_open())
    {
        bool isCurve = false;
        cout << "File open";
        int iteratorLine=0;
        int numberCurves =0;
        vector<array<int,2>> areaZero;
        VectorMatrix *matrix;
        int row, col;
        while(getline(myfile, line)) {
            if (iteratorLine==0){            // --- new file version: the first line also contains the width and height of a cell,
                //     and the second line contains the source position and all links
                size_t pos = 0;
                string token;
                pos = line.find(";");
                string test = line.substr(0, pos);
                string test2 = line.substr(pos + 1, line.length());

                row = stoi(line.substr(0, pos), nullptr, 10);
                line.erase(0, line.find(";") + 1);
                pos = line.find(";");
                col = stoi(line.substr(0, pos), nullptr, 10);
                line.erase(0, line.find(";") + 1);
                pos = line.find(";");
                float subheight = stof( line.substr(0,pos));
                line.erase(0, line.find(";") + 1);
                pos = line.find(";");
                float subwidth =  stof(line.substr(0, pos));
                layers[layerID]->getCurrentLayer()->setWidth(subwidth*(float)col);
                layers[layerID]->getCurrentLayer()->setHeight(subheight*(float)row);
                const QRectF &newBorder = QRectF(sceneRect().center()-QPointF(layers[layerID]->getCurrentLayer()->getWidth()/2., layers[layerID]->getCurrentLayer()->getHeight()/2.),QSize(layers[layerID]->getCurrentLayer()->getWidth(), layers[layerID]->getCurrentLayer()->getHeight()));
                layers[layerID]->getCurrentLayer()->setBorder(newBorder);
                layers[layerID]->setBorderRect(newBorder);
                //layers[layerID]->getCurrentLayer()->setBorder(QRectF(float(this->width())/2.f- float(layers[layerID]->getCurrentLayer()->getWidth())/2.f,float(this->height())/2.f+float(layers[layerID]->getCurrentLayer()->getHeight()), layers[layerID]->getCurrentLayer()->getWidth(), layers[layerID]->getCurrentLayer()->getHeight()));
                matrix = new VectorMatrix(row,col);



                //addLayer(layers[layerID]);
            }


            else if(iteratorLine==1){            // read the second line: contains the source object's position, and possibly information about links
                vector<string> tokens;

                while(line.size()){
                    int index = line.find(";");
                    if(index!=string::npos){
                        tokens.push_back(line.substr(0,index));
                        line = line.substr(index+1);
                        if(line.size()==0)tokens.push_back(line);
                    }else{
                        tokens.push_back(line);
                        line = "";
                    }
                }

                //insert the source
               QPointF sourcePos = layers[layerID]->getCurrentLayer()->boundingRect().topLeft()+ QPointF(std::stof(tokens[0]),std::stof(tokens[1]));
                insertPole(sourcePos,  Pole::Agent, true);

                // read the links one by one
                size_t index = 2;
                while (index + 2 < tokens.size())
                {
                    // link type
                    Link::LinkType linkType;
                    if (tokens[index] == "r")
                        linkType = Link::Rotation;
                    else if (tokens[index] == "s")
                        linkType = Link::Scaling;
                    else if (tokens[index] == "t")
                        linkType = Link::Translation;
                    else
                        std::cout << "error link type";
                        break;
                    // object position
                    QPointF otherObjectPosition = QPointF(std::stof(tokens[index + 1]), std::stof(tokens[index + 2]));
                    insertPole(otherObjectPosition, Pole::Agent, false);
                    // store this link in the result
                    Link *link = new Link(linkType,layers[layerID]->getPoles()[-1], layers[layerID]->getPoles()[-2]);
                    layers[layerID]->addLink(link);
                    scene()->addItem(link);

                    index += 3;
                }
            }


            else if(iteratorLine ==2){
                if(line.find(";") == std::string::npos){
                    isCurve=true;
                    std::istringstream issLine(line);
                    issLine>>numberCurves;
                    getline(myfile, line);
                    iteratorLine ++;
                }
                else{
                    isCurve = false;
                }

             }
            if(isCurve){
                if(iteratorLine < numberCurves +3){ //check curves number
                    size_t pos = 0;
                    float coeffColor= 180;
                    std::string token;
                    std::string delimiter = ";";
                    QList<QPointF> pts;
                    int itCurve = 0;
                        while ((pos = line.find(delimiter)) != std::string::npos) {
                            pos = line.find(delimiter);
                            token = line.substr(0, pos);
                            float posx;
                            float posy;

                            std::istringstream iss(token);
                            if (itCurve ==0){
                               iss>>coeffColor;
                            }
                            else{
                               iss >> posx >> posy ;
                               QPointF p = QPointF(posx,posy);
                               pts.push_back(p+layers[layerID]->boundingRect().topLeft());
                            }

                            std::cout << token << std::endl;
                            line.erase(0, pos + delimiter.length());
                            itCurve ++;

                        }
                        token = line.substr(pos + 1, line.length());
                        float posx;
                        float posy;
                        std::istringstream iss(token);
                        iss >> posx >> posy ;
                        QPointF p = QPointF(posx,posy);
                        pts.push_back(p+layers[layerID]->boundingRect().topLeft());


                    VectorCurve *curvevect = new VectorCurve(pts, layers[layerID]->getCurrentLayer()->getMyPenWidth());
                    layers[layerID]->getCurrentLayer()->addVectorCurve(curvevect); // ass the curve to the layer
                    //add the curve object to the scene
                    scene()->addItem(curvevect);
                    for(auto &node: curvevect->getNodes()){
                        scene()->addItem(node);
                    }
                }
                 else{//read the zero Area, cells that are empty
                        size_t pos = line.find(";");
                        int i = stoi(line.substr(0, pos), nullptr, 10);
                        int j = stoi(line.substr(pos + 1, line.length()), nullptr, 10);
                        areaZero.push_back(array<int,2> {i,j});
                    }

                }
            else if(iteratorLine>1){// of we are is grid reading mode, read each vector
                    int index = iteratorLine-2;
                    int j = index % row;
                    int i = index / col;
                    size_t pos = line.find(";");
                    float x = stof(line.substr(0, pos));
                    float y = stof(line.substr(pos + 1, line.length()));
                    Vector2D vect = Vector2D(x, y);
                    matrix->setAt(i, j, vect);
                    if(vect.isZero()){
                        areaZero.push_back(array<int,2> {i,j});
                    }

                   }
            iteratorLine ++;

            }



        if(areaZero.size()>0){
            layers[layerID]->getCurrentLayer()->setZerosArea(areaZero);
        }
        if(isCurve){
            layers[layerID]->getCurrentLayer()->generateMatrix(brushIntensity);
        }
        else{
            layers[layerID]->getCurrentLayer()->setMatrix(matrix);
        }
        myfile.close();
        update();

    }
}

/**
 * @brief
 *
 */
void RenderArea::zoomIn()
{

    scaleView(qreal(1.2));

}

/**
 * @brief
 *
 */
void RenderArea::zoomOut()
{
    scaleView(1 / qreal(1.2));
}

//! [6]
/**
 * @brief
 *
 * @param painter
 * @param rect
 */
void RenderArea::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    // Shadow
    QRectF sceneRect = this->scene()->sceneRect();


    // Fill
    painter->setPen(QPen(Qt::black, penWidth()));
    painter->drawRect(sceneRect);




}








/**
 * @brief update the field according to the source's size change
 * the curve change according to the change of the obstacle's change
 * @param center current center of the source obstacle
 * @param height current height of the obstacle
 * @param width current wdith of the obstacle
 * @param offsetH difference between the new and old height
 * @param offsetW difference between the new and old width
 */
void RenderArea::updateObstacleSize(QPointF center, float height, float width, float offsetH, float offsetW)
{
    //if the obstacle size change the control curve change accordingly
    for(auto &curve : layers[layerID]->getCurrentLayer()->getVectCurves()){
        for(int i = 0; i < curve->getControlPts().size(); i ++){
            double x = curve->getControlPts()[i].x() +  offsetW*(curve->getControlPts()[i].x()-center.x())/width;
            double y = curve->getControlPts()[i].y() + offsetH*(curve->getControlPts()[i].y()-center.y())/height;
            curve->getControlPts()[i] = QPointF(x,y);
            curve->getNodes()[i]->setPos(QPointF(x,y));

        }
        curve->update();

    }

}




//! [12] //! [13]
//void RenderArea::paintEvent(QPaintEvent *event)
////! [13] //! [14]
//{
//    QPainter painter(this);
//    QRect dirtyRect = event->rect();
//    painter.drawImage(dirtyRect, image, dirtyRect);
//}
//! [14]

//! [15]
//void RenderArea::resizeEvent(QResizeEvent *event)
////! [15] //! [16]
//{
//    if (width() > image.width() || height() > image.height()) {
//        int newWidth = qMax(width() + 128, image.width());
//        int newHeight = qMax(height() + 128, image.height());
//        resizeImage(&image, QSize(newWidth, newHeight));
//        update();
//    }
//    QWidget::resizeEvent(event);
//}
//! [16]

/**
 * @brief
 *
 * @return float
 */
float RenderArea::getBrushIntensity() const
{
    return brushIntensity;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setBrushIntensity(float value)
{
    brushIntensity = value;
}

/**
 * @brief
 *
 * @return int
 */
int RenderArea::getBrushWidth() const
{
    return brushWidth;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setBrushWidth(int value)
{
    brushWidth = value;
    //eraserCursor=cursorBrushUpdate();

}


//! [17]
/**
 * @brief draw a scrible to the finish point
 *
 * @param finishPoint end point of the new line
 */
void RenderArea::drawLineTo(const QPointF &finishPoint) // draw a line to the previous position of the mouse to the next
//! [17] //! [18]
{
     QGraphicsLineItem *line =new QGraphicsLineItem(lastPoint.x(), lastPoint.y(), finishPoint.x(), finishPoint.y());


    QPen pen = QPen(QColor(150,150,150), myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin);

    modified = true;
    float rad = (myPenWidth / 2) + 2;
    scene()->update(QRectF(lastPoint,  finishPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    path.lineTo(finishPoint);
    line->setPen(pen);
    if(scribbleLines!=nullptr && !scribbleLines->childItems().contains(line) && line->line().length()>0)
        scribbleLines->addToGroup(line);
    lastPoint = finishPoint;
    update();
}
/**
 * @brief draw a control curve
 * create a control curve, add it to the current layer viewer and add it to the scene
 * @param l nodes number of the control curve
 * @param p Qpainterpath, i.e the scribble lines drawn during the mouse press and move event.
 */
void RenderArea::drawCurveTo(int l, QPainterPath p) // draw a control curve with l control points, and a path of points which the scribled line previously sketched

//! [17] //! [18]
//!
{
    auto *curveLine = new VectorCurve(p,l, layers[layerID]->getCurrentLayer()->getMyPenWidth()); // create the control curve
    layers[layerID]->getCurrentLayer()->addVectorCurve(curveLine); // add the curve to the curret layer
    layers[layerID]->addInteractionFields(layers[layerID]->getParameter(),layers[layerID]->getCurrentLayer());
    layers[layerID]->getCurrentLayer()->update();
    if(layers[layerID]->getInteractionFields().size()>1)
        layers[layerID]->setIsParametric(true);
    scene()->addItem(curveLine); // add the cruve to the scene
    curveLine->createNodes();// create the nodes

    for(auto &node : curveLine->getNodes() ){ // add the nodes to the scene
        scene()->addItem(node);
     }


}

/**
 * @brief draw a control line
 *  create a control line, add it to the current layer viewer and add it to the scene
 * @param QPainterpath to draw the line, i.e the preview line sketch from the first click mouse point to the current mouse move point
 */
void RenderArea::drawCurveTo( QPainterPath p)

//! [17] //! [18]
//!
{

            scene()->clearSelection();
            float lbis = p.length()/myPenWidth;
            int l = lbis/10;
            if(l>0){
            auto *curveLine = new VectorCurve(p,l, layers[layerID]->getCurrentLayer()->getMyPenWidth());
            layers[layerID]->getCurrentLayer()->addVectorCurve(curveLine);
            layers[layerID]->addInteractionFields(layers[layerID]->getParameter(),layers[layerID]->getCurrentLayer());
            if(layers[layerID]->getInteractionFields().size()>1)
                layers[layerID]->setIsParametric(true);
            //layers[layerID]->addToGroup(curveLine);
            scene()->addItem(curveLine);
            curveLine->setSelected(true);
            curveLine->createNodes();
            for(auto &node : curveLine->getNodes() ){
                scene()->addItem(node);
             }
           }

}


//! [4]

/**
 * @brief
 *
 * @param mode
 */
void RenderArea::setMode(Mode mode)
{
    myMode = mode;
}

/**
 * @brief
 *
 * @param type
 */
void RenderArea::setItemType(Pole::Pole_Type type)
{
    myItemType = type;
}

/**
 * @brief
 *
 * @param type
 */
void RenderArea::setLinkType(Link::LinkType type)
{
    myLinkType = type;
}

//! [14]
/**
 * @brief
 *
 * @param type
 * @return bool
 */
bool RenderArea::isItemChange(int type) const
{
    const QList<QGraphicsItem *> items = scene()->selectedItems();
    const auto cb = [type](const QGraphicsItem *item) { return item->type() == type; };
    return std::find_if(items.begin(), items.end(), cb) != items.end();
}

/**
 * @brief
 *
 * @return float
 */
float RenderArea::getParameter() const
{
    return parameter;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setParameter(float value)
{
    parameter = value;
}


/**
 * @brief
 *
 * @return bool
 */
bool RenderArea::getScaling() const
{
    return scaling;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setScaling(bool value)
{
    scaling = value;
}

/**
 * @brief
 *
 * @return bool
 */
bool RenderArea::getTranslation() const
{
    return translation;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setTranslation(bool value)
{
    translation = value;
}

/**
 * @brief
 *
 * @return bool
 */
bool RenderArea::getRotation() const
{
    return rotation;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setRotation(bool value)
{
    rotation = value;
}

/**
 * @brief
 *
 * @return QList<LayerGroup *>
 */
QList<LayerGroup *> RenderArea::getLayers() const
{
    return layers;
}





/**
 * @brief
 *
 * @return size_t
 */
size_t RenderArea::getLayerID() const
{
    return layerID;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setLayerID(const size_t &value)
{
    layerID = value;
}




/**
 * @brief
 *
 * @param value
 */
void RenderArea::setMyItemMenu(QMenu *value)
{
    myItemMenu = value;
}









/**
 * @brief
 *
 * @return int
 */
int RenderArea::getType() const
{
    return type;
}

/**
 * @brief
 *
 * @param value
 */
void RenderArea::setType(int value)
{
    type = value;
}


void RenderArea::showEvent(QShowEvent *event)
{
    fitInView(sceneRect(), Qt::KeepAspectRatio);
}





/**
 * @brief shhow or hide the current layergroup items
 *
 */
void RenderArea::showLayer(){

    if (layers[layerID]->isVisible()){
        layers[layerID]->hide();
        layers[layerID]->getCurrentLayer()->hideCurves();
        layers[layerID]->hidePoles();
        layers[layerID]->hideLinks();
    }
    else{
        layers[layerID]->show();
        layers[layerID]->getCurrentLayer()->showCurves();
        layers[layerID]->showPoles();
        layers[layerID]->showLinks();
    }

    update();
    scene()->update();


}


/**
 * @brief show or hie the control curves/lines of the current layer group
 *
 */
void RenderArea::showVector(){




    if( layers[layerID]->getCurrentLayer()->getVectCurves().size()>0){
        if (layers[layerID]->isVisible() &&  layers[layerID]->getCurrentLayer()->getVectCurves()[0]->isVisible() ){
            layers[layerID]->getCurrentLayer()->hideCurves();
        }
        else if(layers[layerID]->isVisible() &&  !layers[layerID]->getCurrentLayer()->getVectCurves()[0]->isVisible() ){
            layers[layerID]->getCurrentLayer()->showCurves();
        }

        update();
        scene()->update();

    }


}




/**
 * @brief clear the current layer group
 * delete items, poles, curves etc...
 */
void RenderArea::clearLayer(){
    for (auto &curve : layers[layerID]->getCurrentLayer()->getVectCurves()){
        scene()->removeItem(curve);
        for(auto &node : curve->getNodes())
                scene()->removeItem(node);
    }
    for(auto &pole : layers[layerID]->getPoles()){
        scene()->removeItem(pole);
    }
    for(auto &link : layers[layerID]->getLinks()){
        scene()->removeItem(link);
    }
    for (auto &item : layers[layerID]->childItems()){
        scene()->removeItem(item);
   }
    update();

}

/**
 * @brief add vertical symetrics control vectors of the selected control vectors
 *
 *
 */
void RenderArea::verticalSymetry(){

    for (auto &vect: layers[layerID]->getCurrentLayer()->getVectCurves()){
        if(vect->isSelected()){
            QList<QPointF> pts;
            for(int i =0; i<vect->getNodes().size(); i++){
              Node *node = vect->getNodes()[i];
              pts.push_back(QPointF(layers[layerID]->getCurrentLayer()->getBorder().center().x() - (node->pos().x()-layers[layerID]->getCurrentLayer()->getBorder().center().x()), node->pos().y() ));								
					   
            }
            bool exist =false;
            VectorCurve *newCurve = new VectorCurve(pts,vect->getWidth());
            for (auto &curveBis: layers[layerID]->getCurrentLayer()->getVectCurves()){
                if(curveBis->operator==(*newCurve)){
                    exist = true;
                    break;
                }
            }
            if(!exist){
                layers[layerID]->getCurrentLayer()->addVectorCurve(newCurve);
                scene()->addItem(newCurve);
                vect->setSelected(false);
                newCurve->setSelected(true);
                for(auto &node:newCurve->getNodes()){
                    scene()->addItem(node);
                }
            }
            //generate();to update the matrix each time we add an item
            scene()->update();
        }

    }
}

/**
 * @brief add horizontal symetrics control vectors of the selected control vectors
 *
 *
 */
void RenderArea::horizontalSymetry(){
    for (auto &vect: layers[layerID]->getCurrentLayer()->getVectCurves()){
         if(vect->isSelected()){
             QList<QPointF> pts;
             for(int i =0; i<vect->getNodes().size(); i++){
               Node *node = vect->getNodes()[i];
               pts.push_back(QPointF( node->pos().x(),layers[layerID]->getCurrentLayer()->getBorder().center().y() - (node->pos().y()-layers[layerID]->getCurrentLayer()->getBorder().center().y()) ));

             }
             bool exist = false;
             VectorCurve *newCurve = new VectorCurve(pts,vect->getWidth());
             for (auto &curveBis: layers[layerID]->getCurrentLayer()->getVectCurves()){
                 if(curveBis->operator==(*newCurve)){
                     exist = true;
                     break;
                 }
             }
             if(!exist){
                 layers[layerID]->getCurrentLayer()->addVectorCurve(newCurve);
                 scene()->addItem(newCurve);
                 vect->setSelected(false);
                 newCurve->setSelected(true);
                 for(auto &node:newCurve->getNodes()){
                     scene()->addItem(node);
                 }
                 //generate();to update the matrix each time we add an item
                 scene()->update();
             }

         }

    }
}

/**
 * @brief add angular symetrics control vectors of the selected control vectors
 *
 *
 * @param angle angle to perform symetry from
 */
void RenderArea::angularSymetry(double angle){

    double theta = (360.-angle) *  3.14159265/180.;
    for (auto &vect: layers[layerID]->getCurrentLayer()->getVectCurves()){
        if(vect->isSelected()){
            QList<QPointF> pts;
            for(int i =0; i<vect->getNodes().size(); i++){
              Node *node = vect->getNodes()[i];
              double x_origin = layers[layerID]->getCurrentLayer()->getBorder().center().x();
              double y_origin = layers[layerID]->getCurrentLayer()->getBorder().center().y();
              //Vector2D origin = Vector2D(x_origin, y_origin);

              double x = node->pos().x();
              double y = node->pos().y();
              double x_rotated = ((x - x_origin) * cos(theta)) - ((y - y_origin) * sin(theta)) + x_origin;
              double y_rotated = ((x - x_origin) * sin(theta)) + ((y - y_origin) * cos(theta)) + y_origin;

              pts.push_back(QPointF(x_rotated, y_rotated));

            }
            bool exist = false;
            VectorCurve *newCurve = new VectorCurve(pts,vect->getWidth());
            for (auto &curveBis: layers[layerID]->getCurrentLayer()->getVectCurves()){
                if(curveBis->operator==(*newCurve)){
                    exist=true;
                    break;
                }
            }
            if(!exist){
                layers[layerID]->getCurrentLayer()->addVectorCurve(newCurve);
                scene()->addItem(newCurve);
                vect->setSelected(false);
                newCurve->setSelected(true);
                for(auto &node:newCurve->getNodes()){
                    scene()->addItem(node);
                }
            }
            //generate(); to update the matrix each time we add an item
            scene()->update();

        }

    }
}

/**
 * @brief way of visualizing the result of a fields
 *  show the trajectory of an agent using a fields using Umans csv outputs
 * @param fileName the name of the trajectory csv field
 * @param color color to show the particule trajectory
 */
void RenderArea::getVisualTrajectory(const QString fileName, QColor color){

    string line;

    ifstream myfile(fileName.toUtf8().constData());


    if (myfile.is_open())
    {
        QList<QPointF> pts;
        cout << "File CSV trajectory open ";
        while (getline(myfile, line)) {
            size_t pos = 0;
																						 
														   

            pos = line.find(",");
            line.erase(0,pos + 1);
            pos = line.find(",");
            float x = stof(line.substr(0, pos));
            line.erase(0, pos + 1);
            pos = line.find(",");
            float y = stof( line.substr(0,pos));
            line.erase(0, pos + 1);
            pos = line.find(",");
            QPointF point = QPointF(x ,y) +layers[layerID]->getCurrentLayer()->getBorder().center() ;
            if(pts.empty())
                pts.append(point);
            else if(point !=pts.back()){
                 pts.append(point);
            }

													   

        }

    myfile.close();
    if(pts.length()>0)
        scene()->addItem(new VectorCurve(pts,myPenWidth));


    }



    else {
    cout << "Unable to open file";

    }


}

/**
 * @brief use the previous fuction to show the results of one matrix with several trajectories
 *
 */
void RenderArea::drawComparingMatrix(){
    myPenWidth = layers[layerID]->getCurrentLayer()->getMyPenWidth();
    LayerViewer *layer =  layers[layerID]->getCurrentLayer();
    QRectF border = layer->getBorder();
    gridShow();
    float height = border.height();
    float width = border.width();
    QPointF start = border.bottomLeft();
    //VectorMatrix* GrandTruthMat = getMatrixFomFile("C:\\Users\\acolas\\Documents\\Present\\papierSiggraph\\InteractionField_UserStudy\\Results\\Results_1\\Task1\\matrix\\"+ layers[layerID]->getName()+".txt");
    VectorMatrix matrixMean =VectorMatrix(layer->getMatrix()->getRows(), layer->getMatrix()->getCols());


    for(int k = 2; k<24; k++){
        QString name = layers[layerID]->getName();
        matrixMean += *getMatrixFromFileOld("C:\\Users\\acolas\\Documents\\Present\\papierSiggraph\\InteractionField_UserStudy\\Results\\Results_"+ QString::number(k)+"\\Task7\\matrix\\"+ layers[layerID]->getName()+".txt");
    }

    matrixMean/=22;
    vector<vector<float>>  varianceMatrix ;
    float offseti =height/(float)layer->getMatrix()->getRows();
    float offsetj=width/(float)layer->getMatrix()->getCols();
    for (int i = 0; i< height; i+=offseti){
        vector<float> row;
        for(int j = 0; j<width; j+=offsetj){
            row.push_back(0.f);
        }
        varianceMatrix.push_back(row);
    }
    for(int k = 2; k<24; k++){
        VectorMatrix matrixCompared = *getMatrixFromFileOld("C:\\Users\\acolas\\Documents\\Present\\papierSiggraph\\InteractionField_UserStudy\\Results\\Results_"+ QString::number(k)+"\\Task7\\matrix\\"+ layers[layerID]->getName()+".txt");
        for (int i = 0; i*height/(float)layer->getMatrix()->getRows()< height; i++){
            for(int j = 0; j*width/(float)layer->getMatrix()->getCols()< width; j++){
                //varianceMatrix[i][j] += pow(angle(matrixMean.at(i,j), matrixCompared.at(i,j))*180/PI, 2);
                varianceMatrix[i][j] += (matrixMean.at(i,j)- matrixCompared.at(i,j)).sqrMagnitude();
            }
        }
    }
    for (int i = 0; i*height/(float)layer->getMatrix()->getRows()< height; i++){
        for(int j = 0; j*width/(float)layer->getMatrix()->getCols()< width; j++){

            varianceMatrix[i][j] /= 22;
        }
						  
    }



    for (int i = 0; i*height/(float)layer->getMatrix()->getRows()< height; i++){

        for(int j = 0; j*width/(float)layer->getMatrix()->getCols()< width; j++){
            float y = start.y() - (float)i*height/(float)layer->getMatrix()->getRows();
            float x = start.x()+ (float) j *width/(float)layer->getMatrix()->getCols();
            float yf = y - height/(float)layer->getMatrix()->getRows();
            float xf = x + width/(float)layer->getMatrix()->getCols();
            QRectF rect = QRectF(QPointF(x,y), QPointF(xf,yf));

            QColor color;
            /*
            if( vector.isZero()){
                if(!vectorGD.isZero())
                    color.setRgb(0,0,0);
                else{
                    color.setRgb(255,255,255);
                }
            }
            else if(vectorGD.isZero()){
                color.setRgb(255,0,0);

            }
            else{
               deg = acos(vector.dot(vectorGD)/(vector.magnitude()*vectorGD.magnitude()))*180/PI;
               color.setHsv(220,abs(deg),250);
            }*/


            color.setHsv(220,sqrt(varianceMatrix[i][j])*100,250);
            //color = Qt::gray;

            //QGraphicsEllipseItem *boule = new QGraphicsEllipseItem(QRectF(xRect,yRect,width/(2*(float)layer->getMatrix()->getCols()),height/(2*(float)layer->getMatrix()->getRows())));
            QGraphicsRectItem* carre = new QGraphicsRectItem(rect);
            QBrush brush = QBrush(color);
            brush.setStyle(Qt::SolidPattern);

            carre->setPen(QPen(color,myPenWidth/2.0));
            carre->setBrush(brush);
            scene()->addItem(carre);



        }
    }
    //drawMatrix(GrandTruthMat, QColor(180,50,150));
    drawMatrix(&matrixMean, Qt::black);

    if(layers[layerID]->getName().contains("Vel")){
        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task7\\output_"+QString::number(1)+".csv",  QColor(180,50,150));
        for(int i =2; i<24; i++){

            /*for(int j=3*i-2; j<3*i+1; j++){
                getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task8\\output_"+QString::number(j)+".csv",  QColor(10+i*10,255,180));
                getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task4_1\\output_"+QString::number(j)+".csv",  QColor(10+i*10,255,180));
                getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task4_2\\output_"+QString::number(j)+".csv",  QColor(10+i*10,255,180));
            }*/
              //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_97\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
              //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_23\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
              //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_108\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
              //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_105\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));

            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_3\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_4\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_5\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_1\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_2\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));

             //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6_61\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6_73\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
            //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6_95\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));
              getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task7\\output_"+QString::number(i)+".csv",  QColor(10+i*10,255,180));

        }
    /*

         getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6_61\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
         getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6_73\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
         getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6_95\\output_"+QString::number(1)+".csv",  QColor(134,40,255));*/
        //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_97\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
        //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_23\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
         //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_108\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
         //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task1_105\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
         //getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task6\\output_"+QString::number(1)+".csv",  QColor(134,40,255));*/

        /*for(int j=1; j<4; j++){
            getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task4\\output_"+QString::number(j)+".csv",  QColor(134,40,255));
            getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task4_1\\output_"+QString::number(j)+".csv",  QColor(134,40,255));
            getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task4_2\\output_"+QString::number(j)+".csv",  QColor(134,40,255));
        }

        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_3\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_4\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_5\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_1\\output_"+QString::number(1)+".csv",  QColor(134,40,255));
        getVisualTrajectory("C:\\Users\\acolas\\Documents\\Present\\UMANS\\Build\\ouput\\UserStudy_Task5_2\\output_"+QString::number(1)+".csv",  QColor(134,40,255));*/
    }


}




//! [18]
/**
 * @brief if a layergroup is parametric when the parameter changes the layer displayed must be removed to show the new one
 *
 */
void RenderArea::removeLayerDisplayed(){

    if(!layers[layerID]->getInteractionFields().count(layers[layerID]->getParameter())){
       scene()->destroyItemGroup(layers[layerID]->getCurrentLayer());
    }
    for(auto &item: layers[layerID]->getCurrentLayer()->childItems()){
        scene()->removeItem(item);
        delete item;
    }


    for(auto &vector:layers[layerID]->getCurrentLayer()->getVectCurves()){
        for(auto &node: vector->getNodes()){
               scene()->removeItem(node);
        }
       scene()->removeItem(vector);
   }
      scene()->removeItem(layers[layerID]->getCurrentLayer());
}

/**
 * @brief if a layer group is parametric, add the new parametric field value to be displayed according to the parameter
 *
 */
void RenderArea::addLayerDisplayed(){

    if(layers[layerID]->getInteractionFields().count(layers[layerID]->getParameter())){
        for(auto &vector:layers[layerID]->getCurrentLayer()->getVectCurves()){
            for(auto &node: vector->getNodes()){
                   scene()->addItem(node);
            }
           scene()->addItem(vector);
        }
    }


    drawMatrix();
}

//! [19]
//void RenderArea::resizeImage(QImage *image, const QSize &newSize)
////! [19] //! [20]
//{
//    if (image->size() == newSize)
//        return;

//    QImage newImage(newSize, QImage::Format_RGB32);
//    newImage.fill(qRgb(255, 255, 255));
//    QPainter painter(&newImage);
//    painter.drawImage(QPoint(0, 0), *image);
//    *image = newImage;
//}

