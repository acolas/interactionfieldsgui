#ifndef VECTORLINE_H
#define VECTORLINE_H

#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QPointF>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsObject>
#include <QPainterPath>
#include <vector>
#include <iostream>
#include <string>
#include <pole.h>

class Pole;

class VectorLine : public QGraphicsObject
{
public:

    enum { Type = UserType + 6};
    VectorLine(QGraphicsItem *parent = nullptr);
    VectorLine( const QPointF &sPoint, const QPointF &ePoint,float width_, QGraphicsItem *parent = nullptr);


    QPointF getStartPoint() const;
    void setStartPoint(const QPointF &value);

    QPointF getEndPoint() const;
    void setEndPoint(const QPointF &value);
    QRectF boundingRect() const override;


    void updatePosition();
    int type() const override { return Type; }
    float getWidth() const;
    void setWidth(float value);
    float getLength();


    float getCoeffColor() const;
    void setCoeffColor(float value);

protected:
    //QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

private:
    QPointF startPoint;
    QPointF endPoint;
    qreal arrowSize = 1;
    float width;
    float coeffColor;
    bool change;

};

#endif // VECTORLINE_H
