#include "layerviewer.h"
#include "layergroup.h"
#include "vectorcurve.h"
#include "vectormatrix.h"
#include "vectorline.h"
#include "pole.h"
#include "link.h"
#include "node.h"
#include <QGraphicsItemGroup>
#include <fstream>
#include <QPointF>
#include <QWidget>
#include <QCursor>
#include <QPainterPath>
#include <vector>
#include <array>




/**
 * @brief constructor LayerViewer
 *  ArenderArea contains several layergroup and a layergroup has on LyerViewer. The layer viewer is what is displayer currently for a layerGroup.
 *  If a layer group is parametric, it contains several layerviewr, one per keyframe. One LayerViewer is actually one vector matrix
 * @param width_ width of the layer viewer
 * @param height_ height of the layer viewer
 * @param matrix_ matrix of the layer viewer
 * @param parent
 */
LayerViewer::LayerViewer(float width_, float height_, VectorMatrix *matrix_, QGraphicsItem *parent )
    :QGraphicsItemGroup(parent)
{

    setFlag(QGraphicsItem::ItemIsSelectable , true);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIsMovable, false);
    width = width_;
    height = height_;
    matrix = matrix_;
    border = QRectF(0,0, width_, height_);
    myPenWidth = std::min(border.width()/float(matrix->getRows()), border.height()/float(matrix->getCols()))/10.0f;
    zerosArea = std::vector<std::array<int,2>>();
}

/**
 * @brief other constructor, same arguments
 *
 * @param width_
 * @param height_
 * @param parent
 */
LayerViewer::LayerViewer(float width_, float height_, QGraphicsItem *parent)
    :QGraphicsItemGroup(parent)
{

    setFlag(QGraphicsItem::ItemIsSelectable , false);

    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIsMovable, false);
    width = width_;
    height = height_;
    matrix = new VectorMatrix(20,20);
    border = QRectF(0,0, width_, height_);
    myPenWidth = std::min(border.width()/float(matrix->getRows()), border.height()/float(matrix->getCols()))/10.0f;
    zerosArea = std::vector<std::array<int,2>>();


}

/**
 * @brief
 *
 * @param layer
 */
LayerViewer::LayerViewer(const LayerViewer &layer)
{
    setFlag(QGraphicsItem::ItemIsSelectable , false);

    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIsMovable, false);
    width = layer.getWidth();
    height = layer.getHeight();
    matrix = layer.getMatrix();
    border = layer.getBorder();
    myPenWidth = std::min(border.width()/float(matrix->getRows()), border.height()/float(matrix->getCols()))/10.0f;
    vectCurves = layer.getVectCurves();
    QList<VectorCurve *> vectCurves;
    controlVectors = layer.getControlVectors();
    zerosArea = layer.getZerosArea();
}




/**
 * @brief A layerViewer has a border
 *
 * @return QRectF
 */
QRectF LayerViewer::boundingRect() const
{
    return border;
}



/**
 * @brief check if the layer has been modified since last saved
 *
 * @return bool
 */
bool LayerViewer::getIsModified() const
{
    return isModified;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setIsModified(bool value)
{
    isModified = value;
}

/**
 * @brief
 *
 * @return double
 */
double LayerViewer::getHeight() const
{
    return height;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setHeight(double value)
{
    height = value;
}

/**
 * @brief
 *
 * @return double
 */
double LayerViewer::getWidth() const
{
    return width;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setWidth(double value)
{
    width = value;
}



/**
 * @brief the list of the indexes of the matrix that have been erased
 *
 * @return std::vector<std::array<int, _Tp2> >
 */
std::vector<std::array<int, 2> > LayerViewer::getZerosArea() const
{
    return zerosArea;
}

/**
 * @brief
 *
 * @param std::vector<std::array<int
 * @param value
 */
void LayerViewer::setZerosArea(const std::vector<std::array<int, 2> > &value)
{
    zerosArea = value;
}

/**
 * @brief add a new index to the zero area list
 *
 * @param std::array<int
 * @param ind
 */
void LayerViewer::addZerosArea(const std::array<int, 2> ind)
{
    zerosArea.push_back(ind);

}


/**
 * @brief remove a control line from the control line list
 *
 * @param line control line to be remove
 */
void LayerViewer::removeVectorLine(VectorCurve *line){
    for( QList<VectorCurve*>::iterator iter = vectCurves.begin(); iter != vectCurves.end(); ++iter )
    {
        if( *iter == line )
        {
            vectCurves.erase( iter );
            break;
        }
    }
    //renderArea->getVectLines().erase(std::remove( renderArea->getVectLines().begin(), renderArea->getVectLines().end(), line), renderArea->getVectLines().end());
}


/**
 * @brief the width with which should be draw the control vector on this layerviewer
 *
 * @return double
 */
double LayerViewer::getMyPenWidth() const
{
    return myPenWidth;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setMyPenWidth(double value)
{
    myPenWidth = value;
}
//! [7]



/**
 * @brief parse the control curves and integrate them as control vector.
 * was useful when the magnitude was decided by the control vector but not really anymore (magnitude decided by the brush tool)
 *
 */
void LayerViewer::includeControlVector(){
     matrix->clear();
     controlVectors.clear();
     for (auto &curve : vectCurves){

         //int n = curve->getLinePath().length()/curve->getWidth();
            int n = 40;

         for(int i = 0; i<n-1; i++){
             qreal q1 = float(i)/(n-1);
             qreal q2 = float(i+1)/(n-1);
             QPointF p1 = curve->getLinePath().pointAtPercent(q1);
             QPointF p2 = curve->getLinePath().pointAtPercent(q2);
             VectorLine *line = new VectorLine(p1,p2, curve->getWidth());
             //line->setWidth(line->getWidth()*path.length());

             controlVectors.append(line);//TO DO:erase the distinction between line and curve

         }

     }
     std::vector<std::array<int,2>> matrixControlPoints;

     for(auto &controlLine : controlVectors){

         std::array<int,2> ind = getCase(controlLine->getStartPoint());
         matrixControlPoints.push_back(ind);
         QPointF vectorP = controlLine->getEndPoint()- controlLine->getStartPoint();
         Vector2D vect = Vector2D(vectorP.x(), vectorP.y()).getnormalized()* controlLine->getCoeffColor()/(float)100;


         matrix->setAt(ind[0],ind[1], matrix->at(ind[0],ind[1])+ vect);
     }
     matrix->setControlPoints(matrixControlPoints);

}

/**
 * @brief generate the Vector Matrix
 *  use the liste of the control vector and the postion of the layer viewer to interpolate the vectir matrix
 * @param magnitude the intensity of renderArea brush
 */
void LayerViewer::generateMatrix(float magnitude)
{
    //includeControlVector();

    float unitW = border.width()/(float)getMatrix()->getCols();
    float unitH = border.height()/(float)getMatrix()->getRows();

    //matrix->interpolationInverseDistanceManhattan(1.9);
    std::vector<Vector2D> vectorPos;
    std::vector<Vector2D> vectors;

    for(auto &curve : vectCurves){
        int n = 40;

     for(int i = 0; i<n-1; i++){
         qreal q1 = float(i)/(n-1);
         qreal q2 = float(i+1)/(n-1);
         QPointF p1 = curve->getLinePath().pointAtPercent(q1);
         QPointF p2 = curve->getLinePath().pointAtPercent(q2);
         vectors.push_back(Vector2D((p2-p1).x(), (p2-p1).y()).getnormalized());
         vectorPos.push_back(Vector2D(p1.x(), p1.y()));


     }

    }
    //the zero area stays even if you generate again the matrix, to delete the zero area, use the eras zero area button
    /*for (auto &indices: getZerosArea()){
        if(getMatrix()->at(indices[0], indices[1])==Vector2D(0,0))
            removeZero(indices);
    }*/
      getMatrix()->interpolationInverseDistanceEuclidian(1.9, unitH, unitW, Vector2D(border.bottomLeft().x(),border.bottomLeft().y()), vectorPos, vectors,magnitude );

}
/**
 * @brief add a control curve to the list of control curve
 *
 * @param curve to be added
 */
void LayerViewer::addVectorCurve(VectorCurve *curve)
{

    vectCurves.push_back(curve);
}


/**
 * @brief change the size of the vectorMatrix
 *
 * @param newHeight new dimensions
 * @param newWidth
 */
void LayerViewer::setMatrixSize(int newHeight, int newWidth)
{
    delete(matrix);
    //vectCurves.clear();
    //controlVectors.clear();
    update();
    myPenWidth = std::min(border.width()/float(matrix->getRows()), border.height()/float(matrix->getCols()))/10.0f;
    matrix = new VectorMatrix(newHeight, newWidth);

}

/**
 * @brief clear the layer: delete all control lines and items.
 *
 */
void LayerViewer::clearLayer()
{
    if(matrix!=nullptr)
        matrix->clear();
    if(!vectCurves.isEmpty())
        vectCurves.clear();
    if(!controlVectors.isEmpty())
        controlVectors.clear();
    for(auto item: childItems()){
        scene()->removeItem(item);
        delete (item);
    }
}



/**
 * @brief get the index of the vector matrix for the position of the point position
 *
 * @param point point in the scene
 * @return std::array<int, _Tp2>  return the index (i,j) (line, col) for this position in the scene
 */
std::array<int,2> LayerViewer::getCase(QPointF point)
{
    float height = border.height();
    float width = border.width();
    QPointF start = border.bottomLeft();
    int a=0;
    int b=0;
    bool stop=false;
    std::array<int,2> ind {a,b};
    for (int i = 0; i*height/(float)matrix->getRows()< height; i++){

        for(int j = 0; j*width/(float)matrix->getCols()< width; j++){
            float y = start.y() - (float)i*height/(float)matrix->getRows();
            float x = start.x()+ (float) j *width/(float)matrix->getCols();
            float yf = y - height/(float)matrix->getRows();
            float xf = x + width/(float)matrix->getCols();

            QRectF rect = QRectF(QPointF(x,y), QPointF(xf,yf));
            if(rect.contains(point)){
                ind[0]=i;
                ind[1]=j;
                stop = true;
                break;


            }


        }
        if (stop){
            break;
        }

    }

    return ind;
}
//! [3]
/**
 * @brief save the Interaction Field of this layer Viewer as a matrix
 *
 * @param fileName the name of the IF
 * @param fistLine_ the first line of the field (dimension of the field, type of the source ...)
 * @return bool
 */
bool LayerViewer::saveImage(const QString &fileName, const std::string &fistLine_)
//! [3] //! [4]
{
          std::string firstLine = fistLine_;
          std::ofstream myfile(fileName.toUtf8().constData());
          if (myfile.is_open())
          {

              int r = getMatrix()->getRows();
              int c = getMatrix()->getCols();
              myfile << std::to_string(r) +";" + std::to_string(c) +";" + std::to_string(border.height()/(float)r) +";"+ std::to_string(border.width()/(float)c) +"\n";
              myfile << firstLine;


             // myfile << std::to_string(r) +";" + std::to_string(c)+"\n";

              for( int i =0; i< r; i++){
                  for (int j =0; j< c; j++){
                      bool equal =false;
                      for(auto &ind : getZerosArea()){


                          if(ind == std::array<int,2>{i,j}){
                              equal = true;
                              break;
                           }

                      }
                          if (equal){
                            myfile << std::to_string(0.0f) +";" + std::to_string(0.0f)+"\n";
                          }

                          else{
                            myfile << std::to_string(getMatrix()->at(i,j).x()) +";" + std::to_string(getMatrix()->at(i,j).y())+"\n";
                          }






                    /*
                      if(i==getVectorMatrix()->getSource()[0] && j == getVectorMatrix()->getSource()[1]){
                          myfile << sourceLine;

                          //myfile << "s\n";
                      }
                      else{
                          myfile << std::to_string(getVectorMatrix()->at(i,j).x()) +";" + std::to_string(getVectorMatrix()->at(i,j).y())+"\n";
                      }*/
                  }
              }
            isModified = false;
            myfile.close();

            return true;
          }
          else {
            std::cout << "Unable to open file";
            return false;
          }

    return false;
}


/**
 * @brief save the interaction field as a list of control vector, i.E contrinuus modeling
 *
 * @param fileName name of the field (where to save it)
 * @param fistLine_ first line of the file
 * @return bool
 */
bool LayerViewer::saveImageQuick(const QString &fileName, const std::string &fistLine_)
//! [3] //! [4]
{
          std::string firstLine = fistLine_;
          std::ofstream myfile(fileName.toUtf8().constData());
          if (myfile.is_open())
          {
              int r = getMatrix()->getRows();
              int c = getMatrix()->getCols();
              myfile << std::to_string(r) +";" + std::to_string(c) +";" + std::to_string(border.height()/(float)r) +";"+ std::to_string(border.width()/(float)c) +"\n";
              myfile << firstLine;

              myfile << std::to_string(getVectCurves().size()) +"\n";
              for(auto &curve: getVectCurves()){
                  std::string line = "";
                  for (auto &node: curve->getNodes())
                      line += "1;" + std::to_string(node->pos().x()-getBorder().topLeft().x())+" "+ std::to_string(node->pos().y()-getBorder().topLeft().y());
                  line += "\n";
                  myfile<<line;
              }
              for(auto &indice : zerosArea){

                      //string zeroLine = std::to_string(i)+";"+ std::to_string(j)+"\n";
                      myfile<< std::to_string(indice[0])+";"+ std::to_string(indice[1])+"\n";

              }

            isModified = false;
            myfile.close();

            return true;
          }
          else {
            std::cout << "Unable to open file";
            return false;
          }

    return false;
}
/**
 * @brief list of the vector curves added to this layerviewer
 *
 * @return QList<VectorCurve *>
 */
QList<VectorCurve *> LayerViewer::getVectCurves() const
{
    return vectCurves;
}

/**
 * @brief
 *
 * @param value
 */
void  LayerViewer::setVectCurves(const QList<VectorCurve *> &value)
{
    vectCurves = value;
}
/**
 * @brief if the curve was deleted in the scene, remove the corresponding vector curve in the layer viewer
 *
 * @param curve curve to delete
 */
void  LayerViewer::removeVectorCurve(VectorCurve * curve){


    curve->removeNodes();
    vectCurves.erase(std::remove( vectCurves.begin(), vectCurves.end(), curve), vectCurves.end());
}

/**
 * @brief the vector matrix that define all the vectors of the fields
 *
 * @return VectorMatrix
 */
VectorMatrix * LayerViewer::getMatrix() const
{
    return matrix;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setMatrix(VectorMatrix *value)
{
    matrix = value;
}

/**
 * @brief the border of the layerviewer: limites of the IF
 *
 * @return QRectF
 */
QRectF LayerViewer::getBorder() const
{
    return border;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setBorder(const QRectF &value)
{

    border = value;
    width=border.width();
    height=border.height();
    myPenWidth = std::min(border.width()/float(matrix->getRows()), border.height()/float(matrix->getCols()))/10.0f ;
}
/**
 * @brief the listes of the control lines of the layer viewer
 *
 * @return QList<VectorLine *>
 */
QList<VectorLine *> LayerViewer::getControlVectors() const
{
    return controlVectors;
}

/**
 * @brief
 *
 * @param value
 */
void LayerViewer::setControlVectors(const QList<VectorLine *> &value)
{
    controlVectors = value;
}


/**
 * @brief hide the control curves
 *
 */
void LayerViewer::hideCurves()
{
    for(auto &curve : getVectCurves()){
        for (auto &node : curve->getNodes()){
            node->hide();
        }
        curve->hide();
    }

}

/**
 * @brief show the conrol curves
 *
 */
void LayerViewer::showCurves()
{    for(auto &curve : getVectCurves()){
        for (auto &node : curve->getNodes()){
            node->show();
        }
        curve->show();
    }

}

/**
 * @brief erase one index of the list of null indexes
 *
 * @param std::array<int,2>  indexe to remove
 * @param zero
 */
void LayerViewer::removeZero(std::array<int,2> zero){
     zerosArea.erase(std::remove(zerosArea.begin(), zerosArea.end(), zero), zerosArea.end());

}
/**
 * @brief clear the zeroArea liste
 *
 */
void LayerViewer::removeZeroArea()
{
    // need a copy here since removeLink() will
    // modify the links container
    const auto zerosAreaCopy = zerosArea;

    for (std::array<int,2> zero: zerosAreaCopy) {

       removeZero(zero);
    }
}

/**
 * @brief if 2 object are linked, when the object on which the rotation is deported is moved, preview the changes by moving the control vectors around the source.
 *
 * @param link  link to update
 */
void LayerViewer::updateLink(Link *link){
     if(link->linkType() == Link::Rotation){
    const Vector2D towardEndItemCurrent= Vector2D((link->endItem()->pos()-link->startItem()->pos()).x(),(link->endItem()->pos()-link->startItem()->pos()).y());
    float newAngle = clockwiseAngle( Vector2D(0,-1), towardEndItemCurrent);
        for(auto *curve: vectCurves ){
            std::vector<Node*> nodes = curve->getNodes();
            for(int i =0;i < curve->getNodes().size(); i++){
                const Vector2D newPos = rotateCounterClockwiseAroundPoint(Vector2D( curve->getNodes()[i]->pos().x(),  curve->getNodes()[i]->pos().y()), Vector2D(link->startItem()->pos().x(), link->startItem()->pos().y()), -(newAngle-link->getAngle()));
                curve->getNodes()[i]->setPos(QPointF(newPos.x(), newPos.y()));
                curve->getControlPts()[i]=QPointF(newPos.x(), newPos.y());

            }
                    curve->update();
        }


    link->setAngle(newAngle);
   }

}


