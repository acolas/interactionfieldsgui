
#ifndef NODE_H
#define NODE_H

#include <QGraphicsItem>
#include <QVector>


class VectorLine;
class VectorCurve;

//! [0]
class Node : public QGraphicsItem
{
public:

    Node(QGraphicsItem *parent = nullptr);
    Node(QPointF p, QGraphicsItem *parent = nullptr);

    void addEdge(QLineF *edge);
    QVector<QLineF *> edges() const;

    enum { Type = UserType + 1 };
    int type() const override { return Type; }

    void calculateForces();
    bool advancePosition();

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    int getIndex() const;
    void setIndex(int value);

    VectorCurve *getCurve() const;
    void setCurve(VectorCurve *value);

    void setLine(VectorLine *value);

    VectorLine *getLine() const;
    void updatePosition(QPointF pos);

    bool getManualChange() const;
    void setManualChange(bool value);

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QPointF newPos;
    int index;
    VectorCurve *curve;

    bool manualChange = true;
};
//! [0]

#endif // NODE_H
