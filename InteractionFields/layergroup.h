#ifndef LAYERGROUP_H
#define LAYERGROUP_H



#include "link.h"
#include "pole.h"
#include "layerviewer.h"
#include <QGraphicsItemGroup>
#include <QPointF>
#include <QWidget>
#include <QCursor>
#include <QPainterPath>
#include <vector>
#include <array>

class LayerViewer;

class LayerGroup : public QGraphicsItemGroup
{
public:
    LayerGroup();
    LayerGroup(QString name,size_t id_, bool parametric, bool relatif, QGraphicsItem *parent = nullptr);
    LayerGroup(QString name_,double weight,size_t id_, QGraphicsItem *parent = nullptr);

    void addPole(Pole *pole) ;
    void addLink(Link *link);
    void removePole(Pole *pole);
    void removeLink(Link *link);

    void clearGroup();

    bool getIsHidden() const;
    void setIsHidden(bool value);

    void hidePoles();
    void showPoles();
    void hideLinks();
    void showLinks();
    QList<Pole *> getPoles() const;
    void setPoles(const QList<Pole *> &value);


    QList<Link *> getLinks() const;
    void setLinks(const QList<Link *> &value);



    QString getName() const;
    void setName(const QString &value);


    bool getHasSource() const;
    void setHasSource(bool value);
    std::map<float, LayerViewer*> getInteractionFields() const;
    void setInteractionFields(const std::map<float, LayerViewer*> &value);
    void addInteractionFields(float parameter, LayerViewer *layer);
    LayerViewer computeLayerAt(float parameter) const;
    LayerViewer* getCurrentLayer() const;
    LayerViewer *createNewLayer();


    bool isModified() const;

    bool saveGroup(const QString &fileName, const char *fileFormat, bool isContinuus);


    std::string setMatrixSource();
    float getParameter() const;
    void setParameter(float value);

    bool getIsParametric() const;
    void setIsParametric(bool value);

    bool getIsRelative() const;
    void setIsRelative(bool value);

    QRectF boundingRect() const override;



    void updateLayerBorder();
    QRectF getBorderRect() const;
    void setBorderRect(const QRectF &value);

    void hideVectors();
    void hideLayer();
    bool getIsGridVisible() const;
    void setIsGridVisible(bool value);

    void clear();
    float getCombinationWeight() const;
    void setCombinationWeight(float value);

    void combine(LayerGroup *layerB);
protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;


     void getLayersForInterpolation(const float parameterValue, const LayerViewer *&result0, const LayerViewer *&result1, float &result_mu) const;

     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                QWidget *widget = nullptr) override;

private:
    std::map<float, LayerViewer*> interactionFields;
    bool isRelative;
    bool isParametric ;
    bool isGridVisible;
    float combinationWeight;
    QList<Pole *> poles;
    QList<Link *> links;
    size_t id;
    bool hasSource;
    QPointF previousPos;

    bool isHidden = false;
    QString name = "untitled";
    float parameter = 0.0f;
    QRectF borderRect;


};

#endif // LAYERGROUP_H
