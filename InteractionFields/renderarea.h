#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QObject>
#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainterPath>
#include <vector>
#include <array>
#include <QGenericMatrix>
#include "vectorcurve.h"
#include "vectorline.h"
#include "vectormatrix.h"
#include "layergroup.h"
#include "layerviewer.h"

class RenderArea : public QGraphicsView
{
    Q_OBJECT;
public:

    enum Mode { InsertItem, InsertLine, InsertCurve,InsertLink, MoveItem, EraseItem, ColorBrush };

    RenderArea(QMenu *itemMenu, QWidget *parent = nullptr); // the center widget, the canvas on which the fields are sketched: one for velocity fields and one for orientation fields
    bool openImage(const QString &fileName);
    bool saveImage(const QString &fileName, const char *fileFormat);
    void setPenColor(const QColor &newColor);
    void setPenWidth(float newWidth);
    void setMatrixSize(int newWidth, int newHeight);

    bool isModified() const { return modified; }

    QColor penColor() const { return myPenColor; }
    float penWidth() const { return myPenWidth; }

    void addLayer(LayerGroup *layer);
    QList<VectorCurve *> getVectCurves() const;
    void setVectCurves(const QList<VectorCurve *> &value);
    void gridShow();
    void gridCreate();
    void generateMatrix();
    void drawMatrix();
    void generateMatrixFromFileOld(QString &FileName);
    std::array<int,2> getCase(QPointF);
    void updateObstacleSize(QPointF center, float heigt, float width, float offsetH, float offsetW);


    bool getLineOn() const;
    void setLineOn(bool value);

    bool getCurveOn() const;
    void setCurveOn(bool value);

    void setAgentPlacement(bool value);

    QList<VectorLine *> getVectLines() const;
    void setVectLines(const QList<VectorLine *> &value);


    const VectorMatrix *getVectorMatrix() const { return matrix; }






    void includeControlVector();


    void updateCursor();


    int getType() const;
    void setType(int value);


    void setMyItemMenu(QMenu *value);
    std::string setMatrixSource();




    QSizeF getScreenToWorldScale() const;
    Vector2D screenToWorld(const QPointF &screenPoint) const;
    void removeVectorLine(VectorLine *line);
    void removeVectorCurve(VectorCurve *curve);
    void showEvent(QShowEvent *event) override;

    size_t getLayerID() const;
    void setLayerID(const size_t &value);

    void setLayers(const QList<LayerGroup> &value);

    QList<LayerGroup *> getLayers() const;

    void showLayer();
    void clearLayer();
    bool getRotation() const;
    void setRotation(bool value);

    bool getTranslation() const;
    void setTranslation(bool value);

    bool getScaling() const;
    void setScaling(bool value);

    bool getPole() const;
    void setPole(bool value);

    void verticalSymetry();
    void horizontalSymetry();
    void angularSymetry(double angle);
    void insertPole(QPointF pos, Pole::Pole_Type type, bool source);
    float getParameter() const;
    void setParameter(float value);

    void removeLayerDisplayed();
    void addLayerDisplayed();
    void showVector();
    void generateMatrixFromFile(const QString &fileName);
    void loadField(const QString &fileName);
    void generate();
    bool apply();
    void drawComparingMatrix();
    void generateMatrixFromFileOld(const QString &fileName);
    float getBrushIntensity() const;
    void setBrushIntensity(float value);
    int getBrushWidth() const;
    void setBrushWidth(int value);
    void getVisualTrajectory(const QString fileName, QColor color);
    VectorMatrix *getMatrixFromFileOld(const QString &fileName);
    void drawMatrix(VectorMatrix *matrix, QColor color);
    QCursor cursorBrushUpdate();
    void drawCurveTo(QPainterPath p);
    void checkCombination();
public slots:
    void clearImage();
    //void shuffle();
    void zoomIn();
    void zoomOut();
    void setMode(Mode mode);
    void setItemType(Pole::Pole_Type type);
    void setLinkType(Link::LinkType tuype);





protected:

    void mousePressEvent(QMouseEvent *event) override ;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    //void paintEvent(QGraphicsScenePaintEvent *event) override;
    //void resizeEvent(QResizeEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    //void keyReleaseEvent(QKeyEvent *event) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void drawBackground(QPainter *painter, const QRectF &rect) override;
    void scaleView(qreal scaleFactor);
    void drawRotation(QPointF p1, QPointF p2);
    void drawTranslation(QPointF p1, QPointF p2);
    void drawScaling(QPointF p1, QPointF p2);





signals:
    void itemInserted(Pole *item);
    void itemSelected(QGraphicsItem *item);


private:

    void drawLineTo(const QPointF &endPoint);
    void drawCurveTo(int l, QPainterPath p);
    void resizeImage(QImage *image, const QSize &newSize);
    void drawVecturCuve(const QPointF &startPoint, const QPointF &endPoint);


    bool isItemChange(int type) const;

    Pole::Pole_Type myItemType;
    Link::LinkType myLinkType;
    QMenu *myItemMenu;
    Mode myMode;
    bool leftButtonDown;

    QGraphicsLineItem *line;

    bool modified = false; //if modification has been made sincelast save
    bool scribbling = false; // if we are in scribling mode
    bool rotation = false; // the links
    bool translation = false;
    bool scaling = false;
    bool inCurveSelectMode = false;
    float brushIntensity; // the intensity of the brush i.e the magnitude of the vectors
    int brushWidth; // the amplitude of the brush tool
    float myPenWidth = 0.1;
    size_t layerID; // the ID of the current layer
    size_t nextAgentId;
    size_t nextObstacleId;
    QColor myPenColor ;
    QImage image;//not used, maybe for later uses
    QPointF lastPoint; // the last point of the mouse
    QPointF startPoint;
    QPointF endPoint;
    QPainterPath path;
    QList<LayerGroup *> layers; // the list of layer group on the canvas: one group = one field
    VectorMatrix *matrix;
    QGraphicsItemGroup *scribbleLines; // the lines sribbles
    int type;
    float parameter =0; // if a layer group is parametric (I.E a field is parametric): the current parameter of the current field

    QCursor eraserCursor;






};

#endif // RENDERAREA_H
