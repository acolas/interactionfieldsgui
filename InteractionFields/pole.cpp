#include "pole.h"


#include <QGraphicsScene>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QPainter>
#include <QAction>




/**
 * @brief constructor of the pole
 *
 * @param poleType obstacle or agent
 * @param contextMenu the context menu atrributed to a pole
 * @param s true if the pole is source else false
 * @param parent
 */
Pole::Pole(Pole_Type poleType, QMenu *contextMenu, bool s,
               QGraphicsItem *parent)
: QGraphicsPolygonItem(parent), myPoleType(poleType), isSource(s)
, myContextMenu(contextMenu)

{
    QPainterPath path;
    switch (myPoleType) {
    case Agent:
    {


            setBrush(Qt::black);
            // draw an arrow on top of the disk
            QPainterPath path1;
            path.lineTo(-1, 0);
            path.lineTo(0,1);
            path.lineTo(1,0);
            path.closeSubpath();

            path.moveTo(0,0);
            // draw the disk

           path.addEllipse(QPointF(0,0), 1.5,1.5);
            path.closeSubpath();
            //QColor(0, 0, 0);
            path.setFillRule(Qt::WindingFill);

            //path.addPath(path1);


            myPolygon = path.toFillPolygon();
            typeString ="1";
            break;}
        case Obstacle_square:
        myPolygon << QPointF(-2, -2) << QPointF(2, -2)
                  << QPointF(2, 2) << QPointF(-2, 2)
                  << QPointF(-2, -2);
            typeString = "2";
            break;
        default:
            myPolygon << QPointF(-120, -80) << QPointF(-70, 80)
                      << QPointF(120, 80) << QPointF(70, -80)
                      << QPointF(-120, -80);
            break;
    }
    this->setZValue(1);
    height = 1;
    width = 1;
    setPolygon(myPolygon);
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);

}
//! [0]

//! [1]
/**
 * @brief remove a link attributed to the pole
 *
 * @param link
 */
void Pole::removeLink(Link *link)
{
    links.removeAll(link);
}
//! [1]

//! [2]
/**
 * @brief remove all links
 *
 */
void Pole::removeLinks()
{
    // need a copy here since removeLink() will
    // modify the links container
    const auto linksCopy = links;
    for (Link *link : linksCopy) {
        link->startItem()->removeLink(link);
        link->endItem()->removeLink(link);
        scene()->removeItem(link);
        delete link;
    }
}


//! [2]

//! [3]
/**
 * @brief add a link to the pole
 *
 * @param link
 */
void Pole::addLink(Link *link)
{
    links.append(link);
}
//! [3]

//! [4]
/**
 * @brief the QPixmap that represents the pole in the object menu
 *
 * @return QPixmap
 */
QPixmap Pole::image() const
{
    QPixmap pixmap(70, 70);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);

    painter.setPen(QPen(Qt::black, 0.1));
    painter.scale(20,20);
    painter.translate(1.8, 1.8);
    painter.drawPolyline(myPolygon);

    return pixmap;
}
//! [4]

//! [5]
/**
 * @brief context menu of the pole (right click on it to make it appeared)
 *
 * @param event
 */
void Pole::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    scene()->clearSelection();
    setSelected(true);
    myContextMenu->exec(event->screenPos());


}
//! [5]

//! [6]
/**
 * @brief when a pole is moved around, the links positions attributed to the pole move as well
 *
 * @param change
 * @param value
 * @return QVariant
 */
QVariant Pole::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionChange) {
        for (Link *link : qAsConst(links))
              link->updatePosition();
    }

    return value;
}



/**
 * @brief
 *
 * @return double
 */
double Pole::getWidth() const
{
    return width;
}

/**
 * @brief
 *
 * @param value
 */
void Pole::setWidth(double value)
{
    width = value;
}

/**
 * @brief
 *
 * @return double
 */
double Pole::getHeight() const
{
    return height;
}

/**
 * @brief
 *
 * @param value
 */
void Pole::setHeight(double value)
{
    height = value;
}

/**
 * @brief
 *
 * @return std::string
 */
std::string Pole::getTypeString() const
{
    return typeString;
}

/**
 * @brief change the size of the pole if it is an obstacle
 *
 * @param offsetH
 * @param offsetW
 */
void Pole::changeSize(float offsetH, float offsetW)
{
    if (PoleType() == Obstacle_square) {
        if(height + offsetH >0.5 && width +offsetW >0.5){
            QPolygonF newPolygon;
            height = height + offsetH;
            width = width + offsetW;
            newPolygon << QPointF(-width, -height)<< QPointF(width, -height)
                       <<QPointF(width, height)<<QPointF(-width, height)
                      <<QPointF(-width, -height);
            myPolygon=newPolygon;
            setPolygon(newPolygon);
            update();
        }


    }


}
/**
 * @brief
 *
 * @return size_t
 */
size_t Pole::getId() const
{
    return id;
}

/**
 * @brief
 *
 * @param value
 */
void Pole::setId(size_t value)
{
    id = value;
}

/**
 * @brief
 *
 * @return QVector<Link *>
 */
QVector<Link *> Pole::getLinks() const
{
    return links;
}

/**
 * @brief
 *
 * @return bool
 */
bool Pole::getIsSource() const
{
    return isSource;
}

/**
 * @brief
 *
 * @param value
 */
void Pole::setIsSource(bool value)
{
    isSource = value;
}

/**
 * @brief
 *
 * @param painter
 * @param option
 * @param widget
 */
void Pole::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(PoleType()==Pole::Agent){
        painter->setBrush(Qt::black);
        painter->drawEllipse(-1,-1,2,2);
    }
    QGraphicsPolygonItem::paint(painter, option, widget);
}

//! [6]

